//
//  BMDClippingDetector.m
//  BadMusicDetector
//
//  Created by 堀内 暢之 on 2014/05/22.
//  Copyright (c) 2014年 xsharing. All rights reserved.
//

#import "BMDClippingDetector.h"
#import <AudioToolbox/AudioToolbox.h>
#import <CoreAudio/CoreAudio.h>

@interface BMDClippingDetector () {
	NSMutableArray *clippings;
	NSUInteger clipping_counter;
	float last_value;
}

@end

@implementation BMDClippingDetector
-(void)checkFileAtPath:(NSString *)path callback:(void (^)(NSArray *))callback {
	if (clippings) {
		abort();
	} else {
		clippings = [[NSMutableArray alloc] init];
		clipping_counter = 0;
		last_value = 0;
	}
	//参考: http://objective-audio.jp/2008/05/extendedaudiofile.html
	
    //変換するファイル
    NSString *inPath = path;
	
    //一度に変換するフレーム数
    UInt32 convertFrames = 1024;
	
    //変数の宣言
    OSStatus err = noErr;
    UInt32 size;
    ExtAudioFileRef inAudioFileRef = NULL;
//    ExtAudioFileRef outAudioFileRef = NULL;
    AudioStreamBasicDescription inFileFormat, ioClientFormat, outFileFormat;
    void *ioData = NULL;
	
    //読み込み側のオーディオファイルを開く（2008/6/26修正）
    //NSURL *inUrl = [NSURL URLWithString:inPath];
    NSURL *inUrl = [NSURL fileURLWithPath:inPath];
    err = ExtAudioFileOpenURL((__bridge CFURLRef)inUrl, &inAudioFileRef);
    if (err != noErr) abort();//goto catchErr;
	
    //読み込み側のオーディオファイルからフォーマットを取得する
    //size = sizeof(ioClientFormat);（2009/12/4修正）
    size = sizeof(inFileFormat);
    err = ExtAudioFileGetProperty(
								  inAudioFileRef, kExtAudioFileProperty_FileDataFormat,
								  &size, &inFileFormat);
    if (err != noErr) abort();//goto catchErr;
	
//    //書き出し側のオーディオファイルのパスを作成する（2008/6/26修正）
//    NSString *outPath = [NSString stringWithFormat:@"/tmp/out.wav"];
//    //NSURL *outUrl = [NSURL URLWithString:outPath];
//    NSURL *outUrl = [NSURL fileURLWithPath:outPath];
	
//    //書き出し側のオーディオファイルのフォーマットを作成する
//    outFileFormat.mSampleRate = 22050;
//    outFileFormat.mFormatID = kAudioFormatLinearPCM;
//    outFileFormat.mFormatFlags =
//	kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
//    outFileFormat.mBitsPerChannel = 16;
//    outFileFormat.mChannelsPerFrame = inFileFormat.mChannelsPerFrame;
//    outFileFormat.mFramesPerPacket = 1;
//    outFileFormat.mBytesPerFrame =
//	outFileFormat.mBitsPerChannel / 8 * outFileFormat.mChannelsPerFrame;
//    outFileFormat.mBytesPerPacket =
//	outFileFormat.mBytesPerFrame * outFileFormat.mFramesPerPacket;
//	
//    //書き出し側のオーディオファイルを作成する
//    err = ExtAudioFileCreateWithURL(
//									(__bridge CFURLRef)outUrl, kAudioFileWAVEType, &outFileFormat,
//									NULL, 0, &outAudioFileRef);
    if (err != noErr) goto catchErr;
	
    //読み書き両方のクライアントフォーマットを設定する
    ioClientFormat.mSampleRate = inFileFormat.mSampleRate;
    ioClientFormat.mFormatID = kAudioFormatLinearPCM;
    ioClientFormat.mFormatFlags = kAudioFormatFlagsNativeFloatPacked;
    ioClientFormat.mBitsPerChannel = 32;
    ioClientFormat.mChannelsPerFrame = inFileFormat.mChannelsPerFrame;
    ioClientFormat.mFramesPerPacket = 1;
    ioClientFormat.mBytesPerFrame =
	ioClientFormat.mBitsPerChannel / 8 * ioClientFormat.mChannelsPerFrame;
    ioClientFormat.mBytesPerPacket =
	ioClientFormat.mBytesPerFrame * ioClientFormat.mFramesPerPacket;
	
    size = sizeof(ioClientFormat);
//    err = ExtAudioFileSetProperty(
//								  outAudioFileRef, kExtAudioFileProperty_ClientDataFormat,
//								  size, &ioClientFormat);
    if (err != noErr) goto catchErr;
	
    size = sizeof(ioClientFormat);
    err = ExtAudioFileSetProperty(
								  inAudioFileRef, kExtAudioFileProperty_ClientDataFormat,
								  size, &ioClientFormat);
    if (err != noErr) goto catchErr;
	
    //オーディオデータの読み書きに使用するメモリ領域を確保する
    UInt32 allocByteSize = convertFrames * ioClientFormat.mBytesPerFrame;
    ioData = malloc(allocByteSize);
    if (!ioData) {
        err = 1002;
        goto catchErr;
    }
	
    //オーディオデータの読み書きに使用するAudioBufferListを作成する
    AudioBufferList ioList;
    ioList.mNumberBuffers = 1;
    ioList.mBuffers[0].mNumberChannels = ioClientFormat.mChannelsPerFrame;
    ioList.mBuffers[0].mDataByteSize = allocByteSize;
    ioList.mBuffers[0].mData = ioData;
	
	UInt32 position = 0;
	
    //オーディオデータをコピーする
    while (1) {
        //フレーム数とデータサイズを設定する
        UInt32 frames = convertFrames;
        ioList.mBuffers[0].mDataByteSize = allocByteSize;
		
        //読み込み側のオーディオファイルからオーディオデータを読み込む
        err = ExtAudioFileRead(inAudioFileRef, &frames, &ioList);
        if (err != noErr) goto catchErr;
		
		[self checkFrames:frames ioList:&ioList position:position];
		
        //最後まで読み込んだら終了
        if (frames == 0) break;
		
//        //書き込み側のオーディオファイルへ書き込む
//        err = ExtAudioFileWrite(outAudioFileRef, frames, &ioList);
//        if (err != noErr) goto catchErr;
		
		position += frames;
    }
	
    NSLog(@"complete");
	
	
catchErr:
	
    if (err != noErr) NSLog(@"err = %d", (int)err);
	
    //解放する
    if (ioData) free(ioData);
    if (inAudioFileRef) ExtAudioFileDispose(inAudioFileRef);
//    if (outAudioFileRef) ExtAudioFileDispose(outAudioFileRef);
	
}

-(void)checkFrames:(UInt32) frames ioList:(AudioBufferList*)ioList position:(UInt32)position {
	AudioBuffer *buffer = &(ioList->mBuffers[0]);
	for (UInt32 i=0; i<frames*buffer->mNumberChannels; i+=buffer->mNumberChannels) {
		float current_value = ((float*)buffer->mData)[i];
		float diff = last_value - current_value;
		
		if (((current_value < -0.95) || (0.95 < current_value)) && ((-0.005<diff) && (diff<0.005))) {
			clipping_counter ++;
		} else {
			if (clipping_counter>29) {
				NSLog(@"clipping at %lu for %lu frames, last_value %f", position + i/buffer->mNumberChannels - clipping_counter, (unsigned long)clipping_counter, last_value);
			}
			clipping_counter = 0;
		}
		
		if (current_value) {
			//NSLog(@"%f\n", current_value);
		}
		
		last_value = current_value;
	}
}


@end
