//
//  BMDClippingDetector.h
//  BadMusicDetector
//
//  Created by 堀内 暢之 on 2014/05/22.
//  Copyright (c) 2014年 xsharing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BMDAppDelegate.h"

@interface BMDClippingDetector : NSObject
-(void)checkFileAtPath:(NSString*)path callback:(void(^)(NSArray*))callback;
@end
