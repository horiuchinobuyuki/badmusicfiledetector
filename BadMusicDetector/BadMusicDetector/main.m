//
//  main.m
//  BadMusicDetector
//
//  Created by 堀内 暢之 on 2014/05/22.
//  Copyright (c) 2014年 xsharing. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
	return NSApplicationMain(argc, argv);
}
