//
//  BMDAppDelegate.m
//  BadMusicDetector
//
//  Created by 堀内 暢之 on 2014/05/22.
//  Copyright (c) 2014年 xsharing. All rights reserved.
//

#import "BMDAppDelegate.h"
#import "BMDClippingDetector.h"

@interface BMDAppDelegate () {
	BMDClippingDetector *detector;
}

@end

@implementation BMDAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// Insert code here to initialize your application
	detector = [[BMDClippingDetector alloc] init];
	[detector checkFileAtPath:@"/tmp/test.m4a" callback:^(NSArray * clippings) {
		NSLog(@"%@", clippings);
		
	}];
}

@end
