/******************************************************************************
*  FFT Miscellaneous Routines Version 1.8
******************************************************************************/

#ifndef fftmisc_h
#define fftmisc_h

#include <complex>
#include <cmath>
#include <new>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

#ifndef M_SQRT1_2
#define M_SQRT1_2 0.707106781186547524400844362104849039
#endif

#if (__GNUC__ >= 3)
#define force_inline  __attribute__((always_inline, const))
#define force_inline2 __attribute__((always_inline, pure))
#define force_inline3 __attribute__((always_inline))
#else
#define force_inline
#define force_inline2
#define force_inline3
#endif

#ifdef _MSC_VER

#ifdef USE_AVX
#ifndef __AVX__
#define __AVX__
#endif
#define __SSE3__
#define __SSE2__
#endif

#ifdef USE_SSE3
#define __SSE3__
#define __SSE2__
#endif

#if defined(USE_SSE2) || _M_IX86_FP >= 2
#define __SSE2__
#endif

#ifdef USE_FMA
#define __FMA__
#endif

static inline double rint(const double& x) { return floor(x + 0.5); }

#endif // _MSC_VER

#ifdef __MINGW64__
#undef _MM_MALLOC_H_INCLUDED
#undef _mm_malloc
#undef _mm_free
#include <mm_malloc.h>
#elif defined(__MINGW32__)
#include <malloc.h>
#endif

/*****************************************************************************/

namespace fftmisc {

struct complex_t
{
    double Re, Im;

    complex_t() : Re(0), Im(0) {}
    complex_t(const double& x) : Re(x), Im(0) {}
    complex_t(const double& x, const double& y) : Re(x), Im(y) {}
    complex_t(const std::complex<double>& z) : Re(z.real()), Im(z.imag()) {}
    operator std::complex<double>() { return std::complex<double>(Re, Im); }

    complex_t& operator*=(const double& x)
    {
        Re *= x;
        Im *= x;
        return *this;
    }

    complex_t& operator*=(const complex_t& z)
    {
        const double tmp = Re*z.Re - Im*z.Im;
        Im = Re*z.Im + Im*z.Re;
        Re = tmp;
        return *this;
    }
};

typedef complex_t* __restrict const complex_vector;
typedef const complex_t* __restrict const const_complex_vector;

static inline double Re(const complex_t& z) force_inline;
static inline double Re(const complex_t& z) { return z.Re; }
static inline double Im(const complex_t& z) force_inline;
static inline double Im(const complex_t& z) { return z.Im; }
static inline double norm(const complex_t& z) force_inline;
static inline double norm(const complex_t& z) { return z.Re*z.Re + z.Im*z.Im; }
static inline complex_t conj(const complex_t& z) force_inline;
static inline complex_t conj(const complex_t& z) { return complex_t(z.Re, -z.Im); }
static inline complex_t jx(const complex_t& z) force_inline;
static inline complex_t jx(const complex_t& z) { return complex_t(-z.Im, z.Re); }
static inline complex_t neg(const complex_t& z) force_inline;
static inline complex_t neg(const complex_t& z) { return complex_t(-z.Im, -z.Re); }
static inline complex_t w8x(const complex_t& z) force_inline;
static inline complex_t w8x(const complex_t& z) { return complex_t(M_SQRT1_2*(z.Re-z.Im), M_SQRT1_2*(z.Re-z.Im)); }

static inline complex_t operator+(const complex_t& a, const complex_t& b) force_inline;
static inline complex_t operator+(const complex_t& a, const complex_t& b)
{
    return complex_t(a.Re + b.Re, a.Im + b.Im);
}

static inline complex_t operator-(const complex_t& a, const complex_t& b) force_inline;
static inline complex_t operator-(const complex_t& a, const complex_t& b)
{
    return complex_t(a.Re - b.Re, a.Im - b.Im);
}

static inline complex_t operator*(const double& a, const complex_t& b) force_inline;
static inline complex_t operator*(const double& a, const complex_t& b)
{
    return complex_t(a*b.Re, a*b.Im);
}

static inline complex_t operator*(const complex_t& a, const complex_t& b) force_inline;
static inline complex_t operator*(const complex_t& a, const complex_t& b)
{
    return complex_t(a.Re*b.Re - a.Im*b.Im, a.Re*b.Im + a.Im*b.Re);
}

static inline complex_t operator/(const complex_t& a, const double& b) force_inline;
static inline complex_t operator/(const complex_t& a, const double& b)
{
    return complex_t(a.Re / b, a.Im / b);
}

static inline complex_t operator/(const complex_t& a, const complex_t& b) force_inline;
static inline complex_t operator/(const complex_t& a, const complex_t& b)
{
    const double b2 = b.Re*b.Re + b.Im*b.Im;
    return (a * conj(b)) / b2;
}

static inline complex_t expj(const double& theta) force_inline;
static inline complex_t expj(const double& theta)
{
    return complex_t(cos(theta), sin(theta));
}

} // namespace fftmisc

/*****************************************************************************/

#ifdef __SSE2__

extern "C" {
#include <emmintrin.h>
}

namespace fftmisc {

typedef __m128d xmm;

static inline xmm cmplx(const double& x, const double& y) force_inline;
static inline xmm cmplx(const double& x, const double& y) { return _mm_setr_pd(x, y); }
static inline xmm getpz(const complex_t& z) force_inline;
static inline xmm getpz(const complex_t& z) { return _mm_load_pd(&z.Re); }
static inline void setpz(complex_t& z, const xmm x) force_inline3;
static inline void setpz(complex_t& z, const xmm x) { _mm_store_pd(&z.Re, x); }

static inline xmm cnjpz(const xmm xy) force_inline;
static inline xmm cnjpz(const xmm xy)
{
    static const xmm zm = { 0.0, -0.0 };
    return _mm_xor_pd(zm, xy);
}

static inline xmm jxpz(const xmm xy) force_inline;
static inline xmm jxpz(const xmm xy)
{
    const xmm xmy = cnjpz(xy);
    return _mm_shuffle_pd(xmy, xmy, 1);
}

static inline xmm negpz(const xmm xy) force_inline;
static inline xmm negpz(const xmm xy)
{
    static const xmm mm = { -0.0, -0.0 };
    return _mm_xor_pd(mm, xy);
}

static inline xmm addpz(const xmm a, const xmm b) force_inline;
static inline xmm addpz(const xmm a, const xmm b) { return _mm_add_pd(a, b); }
static inline xmm subpz(const xmm a, const xmm b) force_inline;
static inline xmm subpz(const xmm a, const xmm b) { return _mm_sub_pd(a, b); }
static inline xmm mulpd(const xmm a, const xmm b) force_inline;
static inline xmm mulpd(const xmm a, const xmm b) { return _mm_mul_pd(a, b); }
static inline xmm divpd(const xmm a, const xmm b) force_inline;
static inline xmm divpd(const xmm a, const xmm b) { return _mm_div_pd(a, b); }
static inline xmm xorpd(const xmm a, const xmm b) force_inline;
static inline xmm xorpd(const xmm a, const xmm b) { return _mm_xor_pd(a, b); }

#ifdef __SSE3__

extern "C" {
#include <pmmintrin.h>
}

static inline xmm mulpz(const xmm ab, const xmm xy) force_inline;
static inline xmm mulpz(const xmm ab, const xmm xy)
{
    const xmm aa = _mm_unpacklo_pd(ab, ab);
    const xmm bb = _mm_unpackhi_pd(ab, ab);
    const xmm yx = _mm_shuffle_pd(xy, xy, 1);
    return _mm_addsub_pd(_mm_mul_pd(aa, xy), _mm_mul_pd(bb, yx));
}

static inline xmm divpz(const xmm ab, const xmm xy) force_inline;
static inline xmm divpz(const xmm ab, const xmm xy)
{
    const xmm x2y2 = _mm_mul_pd(xy, xy);
    const xmm r2r2 = _mm_hadd_pd(x2y2, x2y2);
    return _mm_div_pd(mulpz(ab, cnjpz(xy)), r2r2);
}

static inline xmm w8xpz(const xmm xy) force_inline;
static inline xmm w8xpz(const xmm xy)
{
    static const xmm rr = { M_SQRT1_2, M_SQRT1_2 };
    const xmm yx = _mm_shuffle_pd(xy, xy, 1);
    return _mm_mul_pd(rr, _mm_addsub_pd(xy, yx));
}

#else // __SSE2__

static inline xmm mulpz(const xmm ab, const xmm xy) force_inline;
static inline xmm mulpz(const xmm ab, const xmm xy)
{
    const xmm aa = _mm_unpacklo_pd(ab, ab);
    const xmm bb = _mm_unpackhi_pd(ab, ab);
    return _mm_add_pd(_mm_mul_pd(aa, xy), _mm_mul_pd(bb, jxpz(xy)));
}

static inline xmm divpz(const xmm ab, const xmm xy) force_inline;
static inline xmm divpz(const xmm ab, const xmm xy)
{
    const xmm x2y2 = _mm_mul_pd(xy, xy);
    const xmm y2x2 = _mm_shuffle_pd(x2y2, x2y2, 1);
    const xmm r2r2 = _mm_add_pd(x2y2, y2x2);
    return _mm_div_pd(mulpz(ab, cnjpz(xy)), r2r2);
}

static inline xmm w8xpz(const xmm xy) force_inline;
static inline xmm w8xpz(const xmm xy)
{
    static const xmm rr = { M_SQRT1_2, M_SQRT1_2 };
    return _mm_mul_pd(rr, _mm_add_pd(xy, jxpz(xy)));
}

#endif // __SSE3__

#if !defined(__AVX__) && defined(__SSE2__)
static inline void* simd_malloc(int ns) { return _mm_malloc(ns, 16); }
static inline void simd_free(void* p) { _mm_free(p); }
#endif // !__AVX__ && __SSE2__

} // namespace fftmisc

#else // !__SSE2__ && !__SSE3__ && !__AVX__

#include <cstdlib>

namespace fftmisc {

struct xmm { double Re, Im; };

static inline xmm cmplx(const double& x, const double& y) force_inline;
static inline xmm cmplx(const double& x, const double& y)
{
    const xmm z = { x, y };
    return z;
}

static inline const xmm getpz(const complex_t& z) force_inline;
static inline const xmm getpz(const complex_t& z)
{
    const xmm x = { z.Re, z.Im };
    return x;
}

static inline void setpz(complex_t& z, const xmm& x) force_inline3;
static inline void setpz(complex_t& z, const xmm& x) { z.Re = x.Re; z.Im = x.Im; }

static inline xmm cnjpz(const xmm& z) force_inline;
static inline xmm cnjpz(const xmm& z) { const xmm x = { z.Re, -z.Im }; return x; }
static inline xmm jxpz(const xmm& z) force_inline;
static inline xmm jxpz(const xmm& z) { const xmm x = { -z.Im, z.Re }; return x; }
static inline xmm negpz(const xmm& z) force_inline;
static inline xmm negpz(const xmm& z) { const xmm x = { -z.Re, -z.Im }; return x; }

static inline xmm w8xpz(const xmm& z) force_inline;
static inline xmm w8xpz(const xmm& z)
{
    const xmm x = { M_SQRT1_2*(z.Re - z.Im), M_SQRT1_2*(z.Re + z.Im) };
    return x;
}

static inline xmm addpz(const xmm& a, const xmm& b) force_inline;
static inline xmm addpz(const xmm& a, const xmm& b)
{
    const xmm x = { a.Re + b.Re, a.Im + b.Im };
    return x;
}

static inline xmm subpz(const xmm& a, const xmm& b) force_inline;
static inline xmm subpz(const xmm& a, const xmm& b)
{
    const xmm x = { a.Re - b.Re, a.Im - b.Im };
    return x;
}

static inline xmm mulpz(const xmm& a, const xmm& b) force_inline;
static inline xmm mulpz(const xmm& a, const xmm& b)
{
    const xmm x = { a.Re*b.Re - a.Im*b.Im, a.Re*b.Im + a.Im*b.Re };
    return x;
}

static inline xmm divpz(const xmm& a, const xmm& b) force_inline;
static inline xmm divpz(const xmm& a, const xmm& b)
{
    const double b2 = b.Re*b.Re + b.Im*b.Im;
    const xmm acb = mulpz(a, cnjpz(b));
    const xmm x = { acb.Re/b2, acb.Im/b2 };
    return x;
}

static inline xmm mulpd(const xmm& a, const xmm& b) force_inline;
static inline xmm mulpd(const xmm& a, const xmm& b)
{
    const xmm x = { a.Re*b.Re, a.Im*b.Im };
    return x;
}

static inline xmm divpd(const xmm& a, const xmm& b) force_inline;
static inline xmm divpd(const xmm& a, const xmm& b)
{
    const xmm x = { a.Re/b.Re, a.Im/b.Im };
    return x;
}

static inline void* simd_malloc(int ns) { return malloc(ns); }
static inline void simd_free(void* p) { free(p); }

} // namespace fftmisc

#endif // __SSE2__

/*****************************************************************************/

#ifdef __AVX__

extern "C" {
#include <immintrin.h>
}

namespace fftmisc {

typedef __m256d ymm;

static inline ymm cmplx2(const double& a, const double& b, const double& c, const double& d) force_inline;
static inline ymm cmplx2(const double& a, const double& b, const double& c, const double& d)
{
    return _mm256_setr_pd(a, b, c, d);
}

static inline ymm cmplx2(const complex_t& x, const complex_t& y) force_inline;
static inline ymm cmplx2(const complex_t& x, const complex_t& y)
{
#if 1
    const xmm a = getpz(x);
    const xmm b = getpz(y);
    return _mm256_insertf128_pd(_mm256_castpd128_pd256(a), b, 1);
#else
    return _mm256_setr_pd(x.Re, x.Im, y.Re, y.Im);
#endif
}

static inline ymm getpz2(const complex_t* z) force_inline2;
static inline ymm getpz2(const complex_t* z)
{
    return _mm256_load_pd(&z->Re);
}

static inline void setpz2(complex_t* z, const ymm y) force_inline3;
static inline void setpz2(complex_t* z, const ymm y)
{
    _mm256_store_pd(&z->Re, y);
}

static inline ymm cnjpz2(const ymm xy) force_inline;
static inline ymm cnjpz2(const ymm xy)
{
    static const ymm zm = { 0.0, -0.0, 0.0, -0.0 };
    return _mm256_xor_pd(zm, xy);
}

static inline ymm jxpz2(const ymm xy) force_inline;
static inline ymm jxpz2(const ymm xy)
{
    const ymm xmy = cnjpz2(xy);
    return _mm256_shuffle_pd(xmy, xmy, 5);
}

static inline ymm negpz2(const ymm xy) force_inline;
static inline ymm negpz2(const ymm xy)
{
    static const ymm mm = { -0.0, -0.0, -0.0, -0.0 };
    return _mm256_xor_pd(mm, xy);
}

static inline ymm w8xpz2(const ymm xy) force_inline;
static inline ymm w8xpz2(const ymm xy)
{
    static const ymm rr = { M_SQRT1_2, M_SQRT1_2, M_SQRT1_2, M_SQRT1_2 };
    const ymm yx = _mm256_shuffle_pd(xy, xy, 5);
    return _mm256_mul_pd(rr, _mm256_addsub_pd(xy, yx));
}

static inline ymm addpz2(const ymm a, const ymm b) force_inline;
static inline ymm addpz2(const ymm a, const ymm b) { return _mm256_add_pd(a, b); }
static inline ymm subpz2(const ymm a, const ymm b) force_inline;
static inline ymm subpz2(const ymm a, const ymm b) { return _mm256_sub_pd(a, b); }
static inline ymm mulpd2(const ymm a, const ymm b) force_inline;
static inline ymm mulpd2(const ymm a, const ymm b) { return _mm256_mul_pd(a, b); }
static inline ymm divpd2(const ymm a, const ymm b) force_inline;
static inline ymm divpd2(const ymm a, const ymm b) { return _mm256_div_pd(a, b); }

static inline ymm mulpz2(const ymm ab, const ymm xy) force_inline;
static inline ymm mulpz2(const ymm ab, const ymm xy)
{
    const ymm aa = _mm256_unpacklo_pd(ab, ab);
    const ymm bb = _mm256_unpackhi_pd(ab, ab);
    const ymm yx = _mm256_shuffle_pd(xy, xy, 5);
#ifdef __FMA__
    return _mm256_fmaddsub_pd(aa, xy, _mm256_mul_pd(bb, yx));
#else
    return _mm256_addsub_pd(_mm256_mul_pd(aa, xy), _mm256_mul_pd(bb, yx));
#endif // __FMA__
}

static inline ymm divpz2(const ymm ab, const ymm xy) force_inline;
static inline ymm divpz2(const ymm ab, const ymm xy)
{
    const ymm x2y2 = _mm256_mul_pd(xy, xy);
    const ymm r2r2 = _mm256_hadd_pd(x2y2, x2y2);
    return _mm256_div_pd(mulpz2(ab, cnjpz2(xy)), r2r2);
}

static inline ymm duppz2(const xmm x) force_inline;
static inline ymm duppz2(const xmm x) { return _mm256_broadcast_pd(&x); }

template <int s> static inline ymm getwp2(const complex_t* W, const int p) force_inline2;
template <int s> static inline ymm getwp2(const complex_t* W, const int p)
{
    const int sp0 = s * p;
    const int sp1 = s * (p + 1);
    return cmplx2(W[sp0], W[sp1]);
}

template <int s> static inline ymm cnj_getwp2(const complex_t* W, const int p) force_inline2;
template <int s> static inline ymm cnj_getwp2(const complex_t* W, const int p)
{
    const int sp0 = s * p;
    const int sp1 = s * (p + 1);
    const double& ax = W[sp0].Re;
    const double& ay = W[sp0].Im;
    const double& bx = W[sp1].Re;
    const double& by = W[sp1].Im;
    return _mm256_setr_pd(ax, -ay, bx, -by);
}

static inline ymm swaplohi(const ymm a_b) force_inline;
static inline ymm swaplohi(const ymm a_b)
{
    return _mm256_permute2f128_pd(a_b, a_b, 1);
}

static inline xmm getlo(const ymm a_b) force_inline;
static inline xmm getlo(const ymm a_b) { return _mm256_castpd256_pd128(a_b); }
static inline xmm gethi(const ymm a_b) force_inline;
static inline xmm gethi(const ymm a_b) { return _mm256_extractf128_pd(a_b, 1); }

static inline ymm catlohi(const xmm a, const xmm b) force_inline;
static inline ymm catlohi(const xmm a, const xmm b)
{
    return _mm256_insertf128_pd(_mm256_castpd128_pd256(a), b, 1);
}

static inline ymm cnjlohi(const ymm a_b) force_inline;
static inline ymm cnjlohi(const ymm a_b)
{
    static const ymm zzmm = { 0.0, 0.0, -0.0, -0.0 };
    return _mm256_xor_pd(zzmm, a_b);
}

template <int s> static inline ymm getpz3(const complex_t* z) force_inline2;
template <int s> static inline ymm getpz3(const complex_t* z)
{
    const xmm a = getpz(z[0]);
    const xmm b = getpz(z[s]);
    return catlohi(a, b);
}

template <int s> static inline void setpz3(complex_t* z, const ymm y) force_inline3;
template <int s> static inline void setpz3(complex_t* z, const ymm y)
{
    setpz(z[0], getlo(y));
    setpz(z[s], gethi(y));
}

static inline void* simd_malloc(int ns) { return _mm_malloc(ns, 32); }
static inline void simd_free(void* p) { _mm_free(p); }

} // namespace fftmisc

#else // !__AVX__

namespace fftmisc {

struct ymm { xmm lo, hi; };

static inline ymm cmplx2(const double& a, const double& b, const double& c, const double &d) force_inline;
static inline ymm cmplx2(const double& a, const double& b, const double& c, const double &d)
{
    const ymm y = { cmplx(a, b), cmplx(c, d) };
    return y;
}

static inline ymm cmplx2(const complex_t& a, const complex_t& b) force_inline;
static inline ymm cmplx2(const complex_t& a, const complex_t& b)
{
    const ymm y = { getpz(a), getpz(b) };
    return y;
}

static inline ymm getpz2(const complex_t* z) force_inline2;
static inline ymm getpz2(const complex_t* z)
{
    const ymm y = { getpz(z[0]), getpz(z[1]) };
    return y;
}

static inline void setpz2(complex_t* z, const ymm& y) force_inline3;
static inline void setpz2(complex_t* z, const ymm& y)
{
    setpz(z[0], y.lo);
    setpz(z[1], y.hi);
}

static inline ymm cnjpz2(const ymm& xy) force_inline;
static inline ymm cnjpz2(const ymm& xy)
{
    const ymm y = { cnjpz(xy.lo), cnjpz(xy.hi) };
    return y;
}

static inline ymm jxpz2(const ymm& xy) force_inline;
static inline ymm jxpz2(const ymm& xy)
{
    const ymm y = { jxpz(xy.lo), jxpz(xy.hi) };
    return y;
}

static inline ymm w8xpz2(const ymm& xy) force_inline;
static inline ymm w8xpz2(const ymm& xy)
{
    const ymm y = { w8xpz(xy.lo), w8xpz(xy.hi) };
    return y;
}

static inline ymm addpz2(const ymm& a, const ymm& b) force_inline;
static inline ymm addpz2(const ymm& a, const ymm& b)
{
    const ymm y = { addpz(a.lo, b.lo), addpz(a.hi, b.hi) };
    return y;
}

static inline ymm subpz2(const ymm& a, const ymm& b) force_inline;
static inline ymm subpz2(const ymm& a, const ymm& b)
{
    const ymm y = { subpz(a.lo, b.lo), subpz(a.hi, b.hi) };
    return y;
}

static inline ymm mulpz2(const ymm& a, const ymm& b) force_inline;
static inline ymm mulpz2(const ymm& a, const ymm& b)
{
    const ymm y = { mulpz(a.lo, b.lo), mulpz(a.hi, b.hi) };
    return y;
}

static inline ymm divpz2(const ymm& a, const ymm& b) force_inline;
static inline ymm divpz2(const ymm& a, const ymm& b)
{
    const ymm y = { divpz(a.lo, b.lo), divpz(a.hi, b.hi) };
    return y;
}

static inline ymm mulpd2(const ymm& a, const ymm& b) force_inline;
static inline ymm mulpd2(const ymm& a, const ymm& b)
{
    const ymm y = { mulpd(a.lo, b.lo), mulpd(a.hi, b.hi) };
    return y;
}

static inline ymm divpd2(const ymm& a, const ymm& b) force_inline;
static inline ymm divpd2(const ymm& a, const ymm& b)
{
    const ymm y = { divpd(a.lo, b.lo), divpd(a.hi, b.hi) };
    return y;
}

static inline ymm duppz2(const xmm& x) force_inline;
static inline ymm duppz2(const xmm& x) { const ymm y = { x, x }; return y; }

template <int s> static inline ymm getwp2(const complex_t* W, const int p) force_inline2;
template <int s> static inline ymm getwp2(const complex_t* W, const int p)
{
    const int sp0 = s * p;
    const int sp1 = s * (p + 1);
    return cmplx2(W[sp0], W[sp1]);
}

template <int s> static inline ymm cnj_getwp2(const complex_t* W, const int p) force_inline2;
template <int s> static inline ymm cnj_getwp2(const complex_t* W, const int p)
{
    const int sp0 = s * p;
    const int sp1 = s * (p + 1);
    const double& ax = W[sp0].Re;
    const double& ay = W[sp0].Im;
    const double& bx = W[sp1].Re;
    const double& by = W[sp1].Im;
    return cmplx2(ax, -ay, bx, -by);
}

static inline ymm swaplohi(const ymm& a_b) force_inline;
static inline ymm swaplohi(const ymm& a_b)
{
    const ymm b_a = { a_b.hi, a_b.lo };
    return b_a;
}

static inline xmm getlo(const ymm& a_b) force_inline;
static inline xmm getlo(const ymm& a_b) { return a_b.lo; }
static inline xmm gethi(const ymm& a_b) force_inline;
static inline xmm gethi(const ymm& a_b) { return a_b.hi; }

static inline ymm catlohi(const xmm a, const xmm b) force_inline;
static inline ymm catlohi(const xmm a, const xmm b)
{
    const ymm y = { a, b };
    return y;
}

static inline ymm cnjlohi(const ymm& a_b) force_inline;
static inline ymm cnjlohi(const ymm& a_b)
{
    const ymm a_mb = { a_b.lo, negpz(a_b.hi) };
    return a_mb;
}

template <int s> static inline ymm getpz3(const complex_t* z) force_inline2;
template <int s> static inline ymm getpz3(const complex_t* z)
{
    const xmm a = getpz(z[0]);
    const xmm b = getpz(z[s]);
    return catlohi(a, b);
}

template <int s> static inline void setpz3(complex_t* z, const ymm& y) force_inline3;
template <int s> static inline void setpz3(complex_t* z, const ymm& y)
{
    setpz(z[0], getlo(y));
    setpz(z[s], gethi(y));
}

} // namespace fftmisc

#endif // __AVX__

/*****************************************************************************/

namespace fftmisc {

template <class T> struct simd_array
{
    T* p;

    simd_array() : p(0) {}
    simd_array(int n) : p((T*) simd_malloc(n*sizeof(T)))
    {
        if (p == 0) throw std::bad_alloc();
    }

    ~simd_array() { if (p) simd_free(p); }

    void setup(int n)
    {
        if (p) simd_free(p);
        p = (T*) simd_malloc(n*sizeof(T));
        if (p == 0) throw std::bad_alloc();
    }

    void destroy() { if (p) simd_free(p); p = 0; }

    T& operator[](int i) { return p[i]; }
    T operator[](int i) const { return p[i]; }

    T* operator&() { return p; }
};

} // namespace fftmisc

/*****************************************************************************/

#endif // fftmisc_h
