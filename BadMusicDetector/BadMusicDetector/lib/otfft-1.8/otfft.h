/******************************************************************************
*  OTFFT Version 1.8
******************************************************************************/

#ifndef otfft_h
#define otfft_h

#include <cmath>
#include <algorithm>
#include "otfft_difavx.h"
#include "otfft_ditavx.h"
#include "otfft_sixstep.h"

namespace OTFFT { /////////////////////////////////////////////////////////////

using namespace fftmisc;

struct FFT0
{
    int N, log_N;
    OTFFT_DIFAVX::FFT0 fft1;
    OTFFT_DITAVX::FFT0 fft2;
    OTFFT_Sixstep::FFT0 fft3;

    FFT0() : N(0), log_N(0) {}
    FFT0(int n) { setup(n); }

    void setup(int n)
    {
        for (log_N = 0; n > 1; n >>= 1) log_N++;
        setup2(log_N);
    }

    void setup2(const int n)
    {
        log_N = n; N = 1 << n;
#include "otfft_setup.h"
    }

    void fwd(complex_vector x, complex_vector y) const
    {
        if (N < 2) return;
#include "otfft_fwd.h"
        const ymm rN = cmplx2(1.0/N, 1.0/N, 1.0/N, 1.0/N);
        for (int p = 0; p < N; p += 2) setpz2(x+p, mulpd2(rN, getpz2(x+p)));
    }

    void fwd0(complex_vector x, complex_vector y) const
    {
        if (N < 2) return;
#include "otfft_fwd.h"
    }

    void inv(complex_vector x, complex_vector y) const
    {
        if (N < 2) return;
#include "otfft_inv.h"
    }

    void inv0(complex_vector x, complex_vector y) const { inv(x, y); }
};

struct FFT
{
    FFT0 fft;
    simd_array<complex_t> a_y;
    complex_t* y;

    FFT() : fft(), a_y(), y(0) {}
    FFT(const int n) : fft(n), a_y(n), y(&a_y) {}

    void setup(const int n) { fft.setup(n); a_y.setup(n); y = &a_y; }

    void fwd(complex_vector x)  const { fft.fwd(x, y);  }
    void fwd0(complex_vector x) const { fft.fwd0(x, y); }
    void inv(complex_vector x)  const { fft.inv(x, y);  }
    void inv0(complex_vector x) const { fft.inv0(x, y); }
};

void speedup_magic(const int N = 1 << 18)
{
    const double theta0 = 2*M_PI/N;
    volatile double sum = 0;
    for (int p = 0; p < N; p++) {
        sum += cos(p * theta0);
    }
}

} /////////////////////////////////////////////////////////////////////////////

#endif // otfft_h
