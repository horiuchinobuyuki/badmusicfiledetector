/******************************************************************************
*  OTFFT DIFAVX Version 1.8
******************************************************************************/

#ifndef otfft_difavx_h
#define otfft_difavx_h

#include <cmath>
#include "fftmisc.h"

#define OMP_THRESHOLD (1<<14)

namespace OTFFT_DIFAVX { //////////////////////////////////////////////////////

using namespace fftmisc;

template <int n, int s, int eo> struct fwdbut;
template <int n, int s, int eo> struct invbut;

///////////////////////////////////////////////////////////////////////////////

template <int eo> struct fwdbut<2,1,eo>
{
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (eo) {
            const xmm a = getpz(x[0]);
            const xmm b = getpz(x[1]);
            setpz(y[0], addpz(a, b));
            setpz(y[1], subpz(a, b));
        }
        else {
            const xmm a = getpz(x[0]);
            const xmm b = getpz(x[1]);
            setpz(x[0], addpz(a, b));
            setpz(x[1], subpz(a, b));
        }
    }
};

template <int eo> struct invbut<2,1,eo>
{
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (eo) {
            const xmm a = getpz(x[0]);
            const xmm b = getpz(x[1]);
            setpz(y[0], addpz(a, b));
            setpz(y[1], subpz(a, b));
        }
        else {
            const xmm a = getpz(x[0]);
            const xmm b = getpz(x[1]);
            setpz(x[0], addpz(a, b));
            setpz(x[1], subpz(a, b));
        }
    }
};

///////////////////////////////////////////////////////////////////////////////

template <int s, int eo> struct fwdbut<2,s,eo>
{
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (eo) {
            if (s < OMP_THRESHOLD) {
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    complex_vector yq = y + q;
                    const ymm a = getpz2(xq+0);
                    const ymm b = getpz2(xq+s);
                    setpz2(yq+0, addpz2(a, b));
                    setpz2(yq+s, subpz2(a, b));
                }
            }
            else {
                #pragma omp parallel for
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    complex_vector yq = y + q;
                    const ymm a = getpz2(xq+0);
                    const ymm b = getpz2(xq+s);
                    setpz2(yq+0, addpz2(a, b));
                    setpz2(yq+s, subpz2(a, b));
                }
            }
        }
        else {
            if (s < OMP_THRESHOLD) {
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    const ymm a = getpz2(xq+0);
                    const ymm b = getpz2(xq+s);
                    setpz2(xq+0, addpz2(a, b));
                    setpz2(xq+s, subpz2(a, b));
                }
            }
            else {
                #pragma omp parallel for
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    const ymm a = getpz2(xq+0);
                    const ymm b = getpz2(xq+s);
                    setpz2(xq+0, addpz2(a, b));
                    setpz2(xq+s, subpz2(a, b));
                }
            }
        }
    }
};

template <int s, int eo> struct invbut<2,s,eo>
{
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (eo) {
            if (s < OMP_THRESHOLD) {
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    complex_vector yq = y + q;
                    const ymm a = getpz2(xq+0);
                    const ymm b = getpz2(xq+s);
                    setpz2(yq+0, addpz2(a, b));
                    setpz2(yq+s, subpz2(a, b));
                }
            }
            else {
                #pragma omp parallel for
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    complex_vector yq = y + q;
                    const ymm a = getpz2(xq+0);
                    const ymm b = getpz2(xq+s);
                    setpz2(yq+0, addpz2(a, b));
                    setpz2(yq+s, subpz2(a, b));
                }
            }
        }
        else {
            if (s < OMP_THRESHOLD) {
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    const ymm a = getpz2(xq+0);
                    const ymm b = getpz2(xq+s);
                    setpz2(xq+0, addpz2(a, b));
                    setpz2(xq+s, subpz2(a, b));
                }
            }
            else {
                #pragma omp parallel for
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    const ymm a = getpz2(xq+0);
                    const ymm b = getpz2(xq+s);
                    setpz2(xq+0, addpz2(a, b));
                    setpz2(xq+s, subpz2(a, b));
                }
            }
        }
    }
};

///////////////////////////////////////////////////////////////////////////////

template <int eo> struct fwdbut<4,1,eo>
{
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (eo) {
            const xmm a = getpz(x[0]);
            const xmm b = getpz(x[1]);
            const xmm c = getpz(x[2]);
            const xmm d = getpz(x[3]);
            const xmm  apc = addpz(a, c);
            const xmm  amc = subpz(a, c);
            const xmm  bpd = addpz(b, d);
            const xmm jbmd = jxpz(subpz(b, d));
            setpz(y[0], addpz(apc,  bpd));
            setpz(y[1], subpz(amc, jbmd));
            setpz(y[2], subpz(apc,  bpd));
            setpz(y[3], addpz(amc, jbmd));
        }
        else {
            const xmm a = getpz(x[0]);
            const xmm b = getpz(x[1]);
            const xmm c = getpz(x[2]);
            const xmm d = getpz(x[3]);
            const xmm  apc = addpz(a, c);
            const xmm  amc = subpz(a, c);
            const xmm  bpd = addpz(b, d);
            const xmm jbmd = jxpz(subpz(b, d));
            setpz(x[0], addpz(apc,  bpd));
            setpz(x[1], subpz(amc, jbmd));
            setpz(x[2], subpz(apc,  bpd));
            setpz(x[3], addpz(amc, jbmd));
        }
    }
};

template <int eo> struct invbut<4,1,eo>
{
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (eo) {
            const xmm a = getpz(x[0]);
            const xmm b = getpz(x[1]);
            const xmm c = getpz(x[2]);
            const xmm d = getpz(x[3]);
            const xmm  apc = addpz(a, c);
            const xmm  amc = subpz(a, c);
            const xmm  bpd = addpz(b, d);
            const xmm jbmd = jxpz(subpz(b, d));
            setpz(y[0], addpz(apc,  bpd));
            setpz(y[1], addpz(amc, jbmd));
            setpz(y[2], subpz(apc,  bpd));
            setpz(y[3], subpz(amc, jbmd));
        }
        else {
            const xmm a = getpz(x[0]);
            const xmm b = getpz(x[1]);
            const xmm c = getpz(x[2]);
            const xmm d = getpz(x[3]);
            const xmm  apc = addpz(a, c);
            const xmm  amc = subpz(a, c);
            const xmm  bpd = addpz(b, d);
            const xmm jbmd = jxpz(subpz(b, d));
            setpz(x[0], addpz(apc,  bpd));
            setpz(x[1], addpz(amc, jbmd));
            setpz(x[2], subpz(apc,  bpd));
            setpz(x[3], subpz(amc, jbmd));
        }
    }
};

///////////////////////////////////////////////////////////////////////////////

template <int s, int eo> struct fwdbut<4,s,eo>
{
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (eo) {
            if (s < OMP_THRESHOLD) {
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    complex_vector yq = y + q;
                    const ymm a = getpz2(xq+s*0);
                    const ymm b = getpz2(xq+s*1);
                    const ymm c = getpz2(xq+s*2);
                    const ymm d = getpz2(xq+s*3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(yq+s*0, addpz2(apc,  bpd));
                    setpz2(yq+s*1, subpz2(amc, jbmd));
                    setpz2(yq+s*2, subpz2(apc,  bpd));
                    setpz2(yq+s*3, addpz2(amc, jbmd));
                }
            }
            else {
                #pragma omp parallel for
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    complex_vector yq = y + q;
                    const ymm a = getpz2(xq+s*0);
                    const ymm b = getpz2(xq+s*1);
                    const ymm c = getpz2(xq+s*2);
                    const ymm d = getpz2(xq+s*3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(yq+s*0, addpz2(apc,  bpd));
                    setpz2(yq+s*1, subpz2(amc, jbmd));
                    setpz2(yq+s*2, subpz2(apc,  bpd));
                    setpz2(yq+s*3, addpz2(amc, jbmd));
                }
            }
        }
        else {
            if (s < OMP_THRESHOLD) {
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    const ymm a = getpz2(xq+s*0);
                    const ymm b = getpz2(xq+s*1);
                    const ymm c = getpz2(xq+s*2);
                    const ymm d = getpz2(xq+s*3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(xq+s*0, addpz2(apc,  bpd));
                    setpz2(xq+s*1, subpz2(amc, jbmd));
                    setpz2(xq+s*2, subpz2(apc,  bpd));
                    setpz2(xq+s*3, addpz2(amc, jbmd));
                }
            }
            else {
                #pragma omp parallel for
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    const ymm a = getpz2(xq+s*0);
                    const ymm b = getpz2(xq+s*1);
                    const ymm c = getpz2(xq+s*2);
                    const ymm d = getpz2(xq+s*3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(xq+s*0, addpz2(apc,  bpd));
                    setpz2(xq+s*1, subpz2(amc, jbmd));
                    setpz2(xq+s*2, subpz2(apc,  bpd));
                    setpz2(xq+s*3, addpz2(amc, jbmd));
                }
            }
        }
    }
};

template <int s, int eo> struct invbut<4,s,eo>
{
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (eo) {
            if (s < OMP_THRESHOLD) {
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    complex_vector yq = y + q;
                    const ymm a = getpz2(xq+s*0);
                    const ymm b = getpz2(xq+s*1);
                    const ymm c = getpz2(xq+s*2);
                    const ymm d = getpz2(xq+s*3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(yq+s*0, addpz2(apc,  bpd));
                    setpz2(yq+s*1, addpz2(amc, jbmd));
                    setpz2(yq+s*2, subpz2(apc,  bpd));
                    setpz2(yq+s*3, subpz2(amc, jbmd));
                }
            }
            else {
                #pragma omp parallel for
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    complex_vector yq = y + q;
                    const ymm a = getpz2(xq+s*0);
                    const ymm b = getpz2(xq+s*1);
                    const ymm c = getpz2(xq+s*2);
                    const ymm d = getpz2(xq+s*3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(yq+s*0, addpz2(apc,  bpd));
                    setpz2(yq+s*1, addpz2(amc, jbmd));
                    setpz2(yq+s*2, subpz2(apc,  bpd));
                    setpz2(yq+s*3, subpz2(amc, jbmd));
                }
            }
        }
        else {
            if (s < OMP_THRESHOLD) {
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    const ymm a = getpz2(xq+s*0);
                    const ymm b = getpz2(xq+s*1);
                    const ymm c = getpz2(xq+s*2);
                    const ymm d = getpz2(xq+s*3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(xq+s*0, addpz2(apc,  bpd));
                    setpz2(xq+s*1, addpz2(amc, jbmd));
                    setpz2(xq+s*2, subpz2(apc,  bpd));
                    setpz2(xq+s*3, subpz2(amc, jbmd));
                }
            }
            else {
                #pragma omp parallel for
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq = x + q;
                    const ymm a = getpz2(xq+s*0);
                    const ymm b = getpz2(xq+s*1);
                    const ymm c = getpz2(xq+s*2);
                    const ymm d = getpz2(xq+s*3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(xq+s*0, addpz2(apc,  bpd));
                    setpz2(xq+s*1, addpz2(amc, jbmd));
                    setpz2(xq+s*2, subpz2(apc,  bpd));
                    setpz2(xq+s*3, subpz2(amc, jbmd));
                }
            }
        }
    }
};

///////////////////////////////////////////////////////////////////////////////

template <int N, int eo> struct fwdbut<N,1,eo>
{
    static const int N0 = 0;
    static const int N1 = N/4;
    static const int N2 = N/2;
    static const int N3 = N1 + N2;

    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (N < OMP_THRESHOLD) {
            for (int p = 0; p < N/4; p += 2) {
                complex_vector x_p  = x + p;
                complex_vector y_4p = y + 4*p;
                const ymm w1p = getwp2<-1>(W+N,p);
                const ymm w2p = getwp2<-2>(W+N,p);
                const ymm w3p = getwp2<-3>(W+N,p);
                const ymm a = getpz2(x_p+N0);
                const ymm b = getpz2(x_p+N1);
                const ymm c = getpz2(x_p+N2);
                const ymm d = getpz2(x_p+N3);
                const ymm  apc = addpz2(a, c);
                const ymm  amc = subpz2(a, c);
                const ymm  bpd = addpz2(b, d);
                const ymm jbmd = jxpz2(subpz2(b, d));
                setpz3<4>(y_4p+0,             addpz2(apc,  bpd));
                setpz3<4>(y_4p+1, mulpz2(w1p, subpz2(amc, jbmd)));
                setpz3<4>(y_4p+2, mulpz2(w2p, subpz2(apc,  bpd)));
                setpz3<4>(y_4p+3, mulpz2(w3p, addpz2(amc, jbmd)));
            }
        }
        else {
            #pragma omp parallel for
            for (int p = 0; p < N/4; p += 2) {
                complex_vector x_p  = x + p;
                complex_vector y_4p = y + 4*p;
                const ymm w1p = getwp2<-1>(W+N,p);
                const ymm w2p = getwp2<-2>(W+N,p);
                const ymm w3p = getwp2<-3>(W+N,p);
                const ymm a = getpz2(x_p+N0);
                const ymm b = getpz2(x_p+N1);
                const ymm c = getpz2(x_p+N2);
                const ymm d = getpz2(x_p+N3);
                const ymm  apc = addpz2(a, c);
                const ymm  amc = subpz2(a, c);
                const ymm  bpd = addpz2(b, d);
                const ymm jbmd = jxpz2(subpz2(b, d));
                setpz3<4>(y_4p+0,             addpz2(apc,  bpd));
                setpz3<4>(y_4p+1, mulpz2(w1p, subpz2(amc, jbmd)));
                setpz3<4>(y_4p+2, mulpz2(w2p, subpz2(apc,  bpd)));
                setpz3<4>(y_4p+3, mulpz2(w3p, addpz2(amc, jbmd)));
            }
        }
        fwdbut<N/4,4,!eo>()(y, x, W);
    }
};

template <int N, int eo> struct invbut<N,1,eo>
{
    static const int N0 = 0;
    static const int N1 = N/4;
    static const int N2 = N/2;
    static const int N3 = N1 + N2;

    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (N < OMP_THRESHOLD) {
            for (int p = 0; p < N/4; p += 2) {
                complex_vector x_p  = x + p;
                complex_vector y_4p = y + 4*p;
                const ymm w1p = getwp2<1>(W,p);
                const ymm w2p = getwp2<2>(W,p);
                const ymm w3p = getwp2<3>(W,p);
                const ymm a = getpz2(x_p+N0);
                const ymm b = getpz2(x_p+N1);
                const ymm c = getpz2(x_p+N2);
                const ymm d = getpz2(x_p+N3);
                const ymm  apc = addpz2(a, c);
                const ymm  amc = subpz2(a, c);
                const ymm  bpd = addpz2(b, d);
                const ymm jbmd = jxpz2(subpz2(b, d));
                setpz3<4>(y_4p+0,             addpz2(apc,  bpd));
                setpz3<4>(y_4p+1, mulpz2(w1p, addpz2(amc, jbmd)));
                setpz3<4>(y_4p+2, mulpz2(w2p, subpz2(apc,  bpd)));
                setpz3<4>(y_4p+3, mulpz2(w3p, subpz2(amc, jbmd)));
            }
        }
        else {
            #pragma omp parallel for
            for (int p = 0; p < N/4; p += 2) {
                complex_vector x_p  = x + p;
                complex_vector y_4p = y + 4*p;
                const ymm w1p = getwp2<1>(W,p);
                const ymm w2p = getwp2<2>(W,p);
                const ymm w3p = getwp2<3>(W,p);
                const ymm a = getpz2(x_p+N0);
                const ymm b = getpz2(x_p+N1);
                const ymm c = getpz2(x_p+N2);
                const ymm d = getpz2(x_p+N3);
                const ymm  apc = addpz2(a, c);
                const ymm  amc = subpz2(a, c);
                const ymm  bpd = addpz2(b, d);
                const ymm jbmd = jxpz2(subpz2(b, d));
                setpz3<4>(y_4p+0,             addpz2(apc,  bpd));
                setpz3<4>(y_4p+1, mulpz2(w1p, addpz2(amc, jbmd)));
                setpz3<4>(y_4p+2, mulpz2(w2p, subpz2(apc,  bpd)));
                setpz3<4>(y_4p+3, mulpz2(w3p, subpz2(amc, jbmd)));
            }
        }
        invbut<N/4,4,!eo>()(y, x, W);
    }
};

///////////////////////////////////////////////////////////////////////////////

template <int n, int s, int eo> struct fwdbut
{
    static const int N = n*s;
    static const int N0 = 0;
    static const int N1 = N/4;
    static const int N2 = N/2;
    static const int N3 = N1 + N2;

    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (N < OMP_THRESHOLD) {
            for (int p = 0; p < n/4; p++) {
                const int sp = s*p;
                const int s4p = 4*sp;
                const ymm w1p = duppz2(getpz(W[N-1*sp]));
                const ymm w2p = duppz2(getpz(W[N-2*sp]));
                const ymm w3p = duppz2(getpz(W[N-3*sp]));
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq_sp  = x + q + sp;
                    complex_vector yq_s4p = y + q + s4p;
                    const ymm a = getpz2(xq_sp+N0);
                    const ymm b = getpz2(xq_sp+N1);
                    const ymm c = getpz2(xq_sp+N2);
                    const ymm d = getpz2(xq_sp+N3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(yq_s4p+s*0,             addpz2(apc,  bpd));
                    setpz2(yq_s4p+s*1, mulpz2(w1p, subpz2(amc, jbmd)));
                    setpz2(yq_s4p+s*2, mulpz2(w2p, subpz2(apc,  bpd)));
                    setpz2(yq_s4p+s*3, mulpz2(w3p, addpz2(amc, jbmd)));
                }
            }
        }
        else {
            #pragma omp parallel for
            for (int p = 0; p < n/4; p++) {
                const int sp = s*p;
                const int s4p = 4*sp;
                const ymm w1p = duppz2(getpz(W[N-1*sp]));
                const ymm w2p = duppz2(getpz(W[N-2*sp]));
                const ymm w3p = duppz2(getpz(W[N-3*sp]));
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq_sp  = x + q + sp;
                    complex_vector yq_s4p = y + q + s4p;
                    const ymm a = getpz2(xq_sp+N0);
                    const ymm b = getpz2(xq_sp+N1);
                    const ymm c = getpz2(xq_sp+N2);
                    const ymm d = getpz2(xq_sp+N3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(yq_s4p+s*0,             addpz2(apc,  bpd));
                    setpz2(yq_s4p+s*1, mulpz2(w1p, subpz2(amc, jbmd)));
                    setpz2(yq_s4p+s*2, mulpz2(w2p, subpz2(apc,  bpd)));
                    setpz2(yq_s4p+s*3, mulpz2(w3p, addpz2(amc, jbmd)));
                }
            }
        }
        fwdbut<n/4,4*s,!eo>()(y, x, W);
    }
};

template <int n, int s, int eo> struct invbut
{
    static const int N = n*s;
    static const int N0 = 0;
    static const int N1 = N/4;
    static const int N2 = N/2;
    static const int N3 = N1 + N2;

    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W) const
    {
        if (N < OMP_THRESHOLD) {
            for (int p = 0; p < n/4; p++) {
                const int sp = s*p;
                const int s4p = 4*sp;
                const ymm w1p = duppz2(getpz(W[1*sp]));
                const ymm w2p = duppz2(getpz(W[2*sp]));
                const ymm w3p = duppz2(getpz(W[3*sp]));
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq_sp  = x + q + sp;
                    complex_vector yq_s4p = y + q + s4p;
                    const ymm a = getpz2(xq_sp+N0);
                    const ymm b = getpz2(xq_sp+N1);
                    const ymm c = getpz2(xq_sp+N2);
                    const ymm d = getpz2(xq_sp+N3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(yq_s4p+s*0,             addpz2(apc,  bpd));
                    setpz2(yq_s4p+s*1, mulpz2(w1p, addpz2(amc, jbmd)));
                    setpz2(yq_s4p+s*2, mulpz2(w2p, subpz2(apc,  bpd)));
                    setpz2(yq_s4p+s*3, mulpz2(w3p, subpz2(amc, jbmd)));
                }
            }
        }
        else {
            #pragma omp parallel for
            for (int p = 0; p < n/4; p++) {
                const int sp = s*p;
                const int s4p = 4*sp;
                const ymm w1p = duppz2(getpz(W[1*sp]));
                const ymm w2p = duppz2(getpz(W[2*sp]));
                const ymm w3p = duppz2(getpz(W[3*sp]));
                for (int q = 0; q < s; q += 2) {
                    complex_vector xq_sp  = x + q + sp;
                    complex_vector yq_s4p = y + q + s4p;
                    const ymm a = getpz2(xq_sp+N0);
                    const ymm b = getpz2(xq_sp+N1);
                    const ymm c = getpz2(xq_sp+N2);
                    const ymm d = getpz2(xq_sp+N3);
                    const ymm  apc = addpz2(a, c);
                    const ymm  amc = subpz2(a, c);
                    const ymm  bpd = addpz2(b, d);
                    const ymm jbmd = jxpz2(subpz2(b, d));
                    setpz2(yq_s4p+s*0,             addpz2(apc,  bpd));
                    setpz2(yq_s4p+s*1, mulpz2(w1p, addpz2(amc, jbmd)));
                    setpz2(yq_s4p+s*2, mulpz2(w2p, subpz2(apc,  bpd)));
                    setpz2(yq_s4p+s*3, mulpz2(w3p, subpz2(amc, jbmd)));
                }
            }
        }
        invbut<n/4,4*s,!eo>()(y, x, W);
    }
};

///////////////////////////////////////////////////////////////////////////////

struct FFT0
{
    int N, log_N;
    simd_array<complex_t> a_W;
    complex_t* W;

    FFT0() : N(0), log_N(0), W(0) {}
    FFT0(int n) { setup(n); }

    void setup(int n)
    {
        for (log_N = 0; n > 1; n >>= 1) log_N++;
        setup2(log_N);
    }

    void setup2(const int n)
    {
        log_N = n; N = 1 << n;
        a_W.setup(N+1); W = &a_W;
        const double theta0 = 2*M_PI/N;
#if 0
        for (int p = 0; p <= N/2; p++) {
            const double theta = p * theta0;
            const double c = cos(theta);
            const double s = sin(theta);
            W[p] = complex_t(c, s);
            W[N-p] = complex_t(c, -s);
        }
#else
        const int Nh = N/2;
        const int Nq = N/4;
        const int Ne = N/8;
        const int Nd = N - Nq;
        if (N < 2) {}
        else if (N < 4) { W[0] = W[2] = 1; W[1] = complex_t(-1, 0); }
        else if (N < 16) for (int p = 0; p <= Nq; p++) {
            const double theta = p * theta0;
            const double c = cos(theta);
            const double s = sin(theta);
            W[p] = complex_t(c, s);
            W[Nq+p] = complex_t(-s, c);
            W[N-p] = complex_t(c, -s);
            W[Nd-p] = complex_t(-s, -c);
        }
        else for (int p = 0; p <= Ne; p++) {
            const double theta = p * theta0;
            const double c = cos(theta);
            const double s = sin(theta);
            W[p] = complex_t(c, s);
            W[Nq-p] = complex_t(s, c);
            W[Nq+p] = complex_t(-s, c);
            W[Nh-p] = complex_t(-c, s);
            W[N-p] = complex_t(c, -s);
            W[Nd+p] = complex_t(s, -c);
            W[Nd-p] = complex_t(-s, -c);
            W[Nh+p] = complex_t(-c, -s);
        }
#endif
    }

    inline void fwd(complex_vector x, complex_vector y) const
    {
        if (N < 2) return;
        switch (log_N) {
        case  1: fwdbut<(1<< 1),1,0>()(x, y, W); break;
        case  2: fwdbut<(1<< 2),1,0>()(x, y, W); break;
        case  3: fwdbut<(1<< 3),1,0>()(x, y, W); break;
        case  4: fwdbut<(1<< 4),1,0>()(x, y, W); break;
        case  5: fwdbut<(1<< 5),1,0>()(x, y, W); break;
        case  6: fwdbut<(1<< 6),1,0>()(x, y, W); break;
        case  7: fwdbut<(1<< 7),1,0>()(x, y, W); break;
        case  8: fwdbut<(1<< 8),1,0>()(x, y, W); break;
        case  9: fwdbut<(1<< 9),1,0>()(x, y, W); break;
        case 10: fwdbut<(1<<10),1,0>()(x, y, W); break;
        case 11: fwdbut<(1<<11),1,0>()(x, y, W); break;
        case 12: fwdbut<(1<<12),1,0>()(x, y, W); break;
        case 13: fwdbut<(1<<13),1,0>()(x, y, W); break;
        case 14: fwdbut<(1<<14),1,0>()(x, y, W); break;
        case 15: fwdbut<(1<<15),1,0>()(x, y, W); break;
        case 16: fwdbut<(1<<16),1,0>()(x, y, W); break;
        case 17: fwdbut<(1<<17),1,0>()(x, y, W); break;
        case 18: fwdbut<(1<<18),1,0>()(x, y, W); break;
        case 19: fwdbut<(1<<19),1,0>()(x, y, W); break;
        case 20: fwdbut<(1<<20),1,0>()(x, y, W); break;
        case 21: fwdbut<(1<<21),1,0>()(x, y, W); break;
        case 22: fwdbut<(1<<22),1,0>()(x, y, W); break;
        case 23: fwdbut<(1<<23),1,0>()(x, y, W); break;
        case 24: fwdbut<(1<<24),1,0>()(x, y, W); break;
        }
        const ymm rN = cmplx2(1.0/N, 1.0/N, 1.0/N, 1.0/N);
        for (int p = 0; p < N; p += 2) {
            setpz2(x+p, mulpd2(rN, getpz2(x+p)));
        }
    }

    inline void fwd0(complex_vector x, complex_vector y) const
    {
        if (N < 2) return;
        switch (log_N) {
        case  1: fwdbut<(1<< 1),1,0>()(x, y, W); break;
        case  2: fwdbut<(1<< 2),1,0>()(x, y, W); break;
        case  3: fwdbut<(1<< 3),1,0>()(x, y, W); break;
        case  4: fwdbut<(1<< 4),1,0>()(x, y, W); break;
        case  5: fwdbut<(1<< 5),1,0>()(x, y, W); break;
        case  6: fwdbut<(1<< 6),1,0>()(x, y, W); break;
        case  7: fwdbut<(1<< 7),1,0>()(x, y, W); break;
        case  8: fwdbut<(1<< 8),1,0>()(x, y, W); break;
        case  9: fwdbut<(1<< 9),1,0>()(x, y, W); break;
        case 10: fwdbut<(1<<10),1,0>()(x, y, W); break;
        case 11: fwdbut<(1<<11),1,0>()(x, y, W); break;
        case 12: fwdbut<(1<<12),1,0>()(x, y, W); break;
        case 13: fwdbut<(1<<13),1,0>()(x, y, W); break;
        case 14: fwdbut<(1<<14),1,0>()(x, y, W); break;
        case 15: fwdbut<(1<<15),1,0>()(x, y, W); break;
        case 16: fwdbut<(1<<16),1,0>()(x, y, W); break;
        case 17: fwdbut<(1<<17),1,0>()(x, y, W); break;
        case 18: fwdbut<(1<<18),1,0>()(x, y, W); break;
        case 19: fwdbut<(1<<19),1,0>()(x, y, W); break;
        case 20: fwdbut<(1<<20),1,0>()(x, y, W); break;
        case 21: fwdbut<(1<<21),1,0>()(x, y, W); break;
        case 22: fwdbut<(1<<22),1,0>()(x, y, W); break;
        case 23: fwdbut<(1<<23),1,0>()(x, y, W); break;
        case 24: fwdbut<(1<<24),1,0>()(x, y, W); break;
        }
    }

    inline void inv(complex_vector x, complex_vector y) const
    {
        if (N < 2) return;
        switch (log_N) {
        case  1: invbut<(1<< 1),1,0>()(x, y, W); break;
        case  2: invbut<(1<< 2),1,0>()(x, y, W); break;
        case  3: invbut<(1<< 3),1,0>()(x, y, W); break;
        case  4: invbut<(1<< 4),1,0>()(x, y, W); break;
        case  5: invbut<(1<< 5),1,0>()(x, y, W); break;
        case  6: invbut<(1<< 6),1,0>()(x, y, W); break;
        case  7: invbut<(1<< 7),1,0>()(x, y, W); break;
        case  8: invbut<(1<< 8),1,0>()(x, y, W); break;
        case  9: invbut<(1<< 9),1,0>()(x, y, W); break;
        case 10: invbut<(1<<10),1,0>()(x, y, W); break;
        case 11: invbut<(1<<11),1,0>()(x, y, W); break;
        case 12: invbut<(1<<12),1,0>()(x, y, W); break;
        case 13: invbut<(1<<13),1,0>()(x, y, W); break;
        case 14: invbut<(1<<14),1,0>()(x, y, W); break;
        case 15: invbut<(1<<15),1,0>()(x, y, W); break;
        case 16: invbut<(1<<16),1,0>()(x, y, W); break;
        case 17: invbut<(1<<17),1,0>()(x, y, W); break;
        case 18: invbut<(1<<18),1,0>()(x, y, W); break;
        case 19: invbut<(1<<19),1,0>()(x, y, W); break;
        case 20: invbut<(1<<20),1,0>()(x, y, W); break;
        case 21: invbut<(1<<21),1,0>()(x, y, W); break;
        case 22: invbut<(1<<22),1,0>()(x, y, W); break;
        case 23: invbut<(1<<23),1,0>()(x, y, W); break;
        case 24: invbut<(1<<24),1,0>()(x, y, W); break;
        }
    }

    inline void inv0(complex_vector x, complex_vector y) const { inv(x, y); }
};

struct FFT
{
    FFT0 fft;
    simd_array<complex_t> a_y;
    complex_t* y;

    FFT() : y(0) {}
    FFT(const int n) : fft(n), a_y(n), y(&a_y) {}

    inline void setup(const int n) { fft.setup(n); a_y.setup(n); y = &a_y; }

    inline void fwd(complex_vector x)  const { fft.fwd(x, y);  }
    inline void fwd0(complex_vector x) const { fft.fwd0(x, y); }
    inline void inv(complex_vector x)  const { fft.inv(x, y);  }
    inline void inv0(complex_vector x) const { fft.inv0(x, y); }
};

void speedup_magic(int N = 1 << 18)
{
    const double theta0 = 2*M_PI/N;
    volatile double sum = 0;
    for (int p = 0; p < N; p++) {
        sum += cos(p * theta0);
    }
}

} /////////////////////////////////////////////////////////////////////////////

#endif // otfft_difavx_h
