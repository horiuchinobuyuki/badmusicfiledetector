　OTFFT は無保証です。OTFFT を使った結果どのような損害を被っても、作者
OKおじさんは一切責任を負いません。自己責任でご利用ください。

　OTFFT は自由に使ってくださって結構ですが、使ったプログラムを配布あるいは
販売する場合は、OKおじさんの OTFFT を使ったことをドキュメントに記載して
ください。改変して配布・販売する場合も同様です。

　以前は OTFFT を OKFFT の名前で公開していました。しかし、Google で検索して
みると、既に OKFFT のキーワードがたくさんヒットしました。こりゃまずいと思い、
名前を OTFFT(OK おじさん Template FFT)に変更しました。古い OKFFT をダウンロード
した人も、配布・販売する場合はこのドキュメントの記載にしたがってください。

　さて、いったい何人の方が使ってくださるか分かりませんが、一応私の提案する
OTFFT の使い方について説明しておきましょう。otfft-1.tar.gz を展開すると
いくつかのファイルができますが、OTFFT を使うのに必要なファイルはその中の

    Makefile
    fftmisc.h
    ffttune.cpp
    otfft.h
    otfft_difavx.h
    otfft_ditavx.h
    otfft_fwd.h
    otfft_inv.h
    otfft_setup.h
    otfft_sixstep.h

です。このうち、otfft_fwd.h, otfft_inv.h, otfft_setup.h は自動生成される
ファイルで、自分の環境で最大の効果を発揮したければ ffttune を使って生成し
なおしてやる必要があります。まず、

    make ffttune

で ffttune コマンドを作ります。次に

    ./ffttune

と ffttune コマンドを実行します。すると先に示したファイルの自動生成が
始まります。しばらく時間がかかるのでお待ちください。

　さて、自動生成が終わればあとは otfft.h をインクルードするだけで OTFFT は
使えます。GCC でコンパイルするには -march=native -mtune=native -fopenmp を
付けてください。Visual C++ の場合 AVX を使うには、/arch:AVX /DUSE_AVX を指定
してください。OpenMP も使うには /openmp も指定してください。サイズ N の FFT
を実行するには、

    using fftmisc::complex_t;
    using fftmisc::simd_malloc;
    using fftmisc::simd_free;
    complex_t* x = (complex_t*) simd_malloc(N*sizeof(complex_t));
    ...
    OTFFT::FFT fft(N);
    fft.fwd(x);
    ...
    simd_free(x);

のようにします。N は最大2の24乗までで、2のべき乗である必要があります。
complex_t は

    struct complex_t
    {
        double Re, Im;

        complex_t() : Re(0), Im(0) {}
        complex_t(const double& x) : Re(x), Im(0) {}
        complex_t(const double& x, const double& y) : Re(x), Im(y) {}
        complex_t(const std::complex<double>& z) : Re(z.real()), Im(z.imag()) {}
        operator std::complex<double>() { return std::complex<double>(Re, Im); }

        complex_t& operator*=(const double& x)
        {
            Re *= x;
            Im *= x;
            return *this;
        }
    };

のように定義されています。

　フーリエ変換(fwd)には係数 1/N が掛かっています。もしそれが不都合なら係数の
掛かっていない(fwd0)もあります。逆変換したい場合は、

    fft.inv(x);

のようにします。fft のサイズを変更したい場合は、

    fft.setup(2 * N);

のようにします。

　OTFFT は Stockham のアルゴリズムを使っていますので、実行には入力系列と
同じサイズの作業領域が必要です。普通は内部でその領域を確保しますが、
マルチスレッドのプログラムを組む場合など、それだと都合が悪い場合があります。
そんな時は外部から作業領域を渡すバージョンもあります。次のように使います。

    using fftmisc::complex_t;
    using fftmisc::simd_malloc;
    using fftmisc::simd_free;
    complex_t* x = (complex_t*) simd_malloc(N*sizeof(complex_t));
    complex_t* y = (complex_t*) simd_malloc(N*sizeof(complex_t));
    ...
    OTFFT::FFT0 fft(N);
    fft.fwd(x, y); // y が作業領域
    fft.inv(x, y); // y が作業領域
    ...
    simd_free(y);
    simd_free(x);

　OTFFT::FFT だったところが OTFFT::FFT0 になっていることに注意してください。

　最後に、OTFFT にはベンチマークプログラムが付属しています。しかし、この
ベンチマークには FFTW3 と大浦さんのプログラムを用いているため、単独では動作
しません。まずは FFTW3 をインストールし、大浦さんのページ

    http://www.kurims.kyoto-u.ac.jp/~ooura/fft-j.html

から大浦さんの FFT ライブラリをダウンロードしてください。そしてその中の
fftsg.c というファイルを fftbench1.cpp と同じフォルダに入れてください。
その上で以下のように fftbench1 コマンドと fftbench2 コマンドを make します。

    make fftbench1
    make fftbench2

fftbench1 が計測時間に FFT オブジェクトの初期化を含まないベンチマーク、
fftbench2 が初期化を含むベンチマークです。以下のように実行してください。

    ./fftbench1
    ./fftbench2
