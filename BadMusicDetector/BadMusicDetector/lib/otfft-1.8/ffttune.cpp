/******************************************************************************
*  FFT Tuning Command
******************************************************************************/

#include <cstdio>
#include <cmath>
#include "stopwatch.h"
#include "fftmisc.h"
#include "otfft_difavx.h"
#include "otfft_ditavx.h"
#include "otfft_sixstep.h"
using namespace fftmisc;

#define DELAY 1

int main() try
{
    static const int n_max  = 22;
    static const int N_max  = 1 << n_max;
    static const int Nn_max = N_max * n_max;

    setbuf(stdout, NULL);
    FILE* fp1 = fopen("otfft_setup.h", "w");
    FILE* fp2 = fopen("otfft_fwd.h", "w");
    FILE* fp3 = fopen("otfft_inv.h", "w");
    fprintf(fp1, "switch (log_N) {\n");
    fprintf(fp2, "switch (log_N) {\n");
    fprintf(fp3, "switch (log_N) {\n");
    complex_t* x = (complex_t*) simd_malloc(N_max*sizeof(complex_t));
    complex_t* y = (complex_t*) simd_malloc(N_max*sizeof(complex_t));
    for (int n = 1; n <= n_max; n++) {
        const int N = 1 << n;
        const int LOOPS = static_cast<int>(rint(Nn_max/double(N*n)));
        counter_t cp1, cp2;
        double lap, tmp;
        OTFFT_DIFAVX::FFT0 fft1(N);
        OTFFT_DITAVX::FFT0 fft2(N);
        OTFFT_Sixstep::FFT0 fft3(N);
        int fft_num = 1;

        for (int p = 0; p < N; p++) {
            const double t = double(p) / N;
            x[p].Re = cos(5*(2*M_PI/N)*t*t);
            x[p].Im = sin(5*(2*M_PI/N)*t*t);
        }
        
        OTFFT_DIFAVX::speedup_magic();
        cp1 = get_counter();
        for (int i = 0; i < LOOPS; i++) {
            fft1.fwd(x, y);
            fft1.inv(x, y);
        }
        cp2 = get_counter();
        lap = usec(cp2 - cp1);

        sleep(DELAY);

        OTFFT_DITAVX::speedup_magic();
        cp1 = get_counter();
        for (int i = 0; i < LOOPS; i++) {
            fft2.fwd(x, y);
            fft2.inv(x, y);
        }
        cp2 = get_counter();
        tmp = usec(cp2 - cp1);
        if (tmp < lap) { lap = tmp; fft_num = 2; }

        sleep(DELAY);

        OTFFT_Sixstep::speedup_magic();
        cp1 = get_counter();
        for (int i = 0; i < LOOPS; i++) {
            fft3.fwd(x, y);
            fft3.inv(x, y);
        }
        cp2 = get_counter();
        tmp = usec(cp2 - cp1);
        if (tmp < lap) { lap = tmp; fft_num = 3; }

        sleep(DELAY);

        fprintf(fp1, "case %2d: fft%d.setup2(log_N); break;\n", n, fft_num);
        fprintf(fp2, "case %2d: fft%d.fwd0(x, y); break;\n", n, fft_num);
        fprintf(fp3, "case %2d: fft%d.inv0(x, y); break;\n", n, fft_num);
        printf("2^(%2d): fft%d\n", n, fft_num);
    }
    simd_free(y);
    simd_free(x);
    fprintf(fp1, "default: fft1.setup2(log_N); break;\n");
    fprintf(fp2, "default: fft1.fwd0(x, y); break;\n");
    fprintf(fp3, "default: fft1.inv0(x, y); break;\n");
    fprintf(fp1, "}\n");
    fprintf(fp2, "}\n");
    fprintf(fp3, "}\n");
    fclose(fp3);
    fclose(fp2);
    fclose(fp1);

    return 0;
}
catch (std::bad_alloc&) { fprintf(stderr, "\n""not enough memory!!\n"); }
