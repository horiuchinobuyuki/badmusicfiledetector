/******************************************************************************
*  OOURA FFT 1
******************************************************************************/

#ifndef ooura1_h
#define ooura1_h

#include <cmath>
#include "fftmisc.h"

#ifdef _MSC_VER
#define USE_CDFT_WINTHREADS
#else
#define USE_CDFT_PTHREADS
#endif

#include "fftsg.c"
//#include "fft4g.c"
//#include "fft8g.c"

namespace OOURA { /////////////////////////////////////////////////////////////

using namespace fftmisc;

class FFT
{
private:
    int N;
    simd_array<int> a_ip;
    simd_array<double> a_w;
    simd_array<complex_t> a_y;
    int* ip;
    double* w;
    complex_t* y;
public:
    FFT(const int n) : N(n),
        a_ip(2 + int(ceil(sqrt(double(N))))),
        a_w(int(ceil(N / 2.0))), a_y(N),
        ip(&a_ip), w(&a_w), y(&a_y)
    {
        ip[0] = 0;
        cdft(2*N, -1, &y->Re, ip, w);
        a_y.destroy();
    }

    void fwd(complex_t* const x) const
    {
        cdft(2*N, -1, &x->Re, ip, w);
        const ymm rN = cmplx2(1.0/N, 1.0/N, 1.0/N, 1.0/N);
        if (N > 1)
            for (int k = 0; k < N; k += 2) setpz2(x+k, mulpd2(rN, getpz2(x+k)));
    }

    void inv(complex_t* const x) const
    {
        cdft(2*N, 1, &x->Re, ip, w);
    }
};

} /////////////////////////////////////////////////////////////////////////////

#endif // ooura1_h
