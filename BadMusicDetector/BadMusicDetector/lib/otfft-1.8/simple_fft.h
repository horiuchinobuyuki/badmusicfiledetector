/******************************************************************************
* Simple FFT (Cooley Tukey 4 radix)
******************************************************************************/

#ifndef simple_fft_h
#define simple_fft_h

#include <algorithm>
#include "fftmisc.h"

namespace SimpleFFT2 { ////////////////////////////////////////////////////////

using namespace fftmisc;

static inline void fwdbut(int N, complex_t* x, complex_t* W)
{
    int n = N;
    for (int s = 1; n > 2; n /= 4, s *= 4) {
        const int n0 = 0;
        const int n1 = n / 4;
        const int n2 = n / 2;
        const int n3 = n1 + n2;
        for (int q = 0; q < N; q += n) {
            complex_t* const xq = x + q;
            for (int p = 0; p < n1; p++) {
                const complex_t w1p = conj(W[1*s*p]);
                const complex_t w2p = conj(W[2*s*p]);
                const complex_t w3p = conj(W[3*s*p]);
                const complex_t a = xq[p+n0];
                const complex_t b = xq[p+n1];
                const complex_t c = xq[p+n2];
                const complex_t d = xq[p+n3];
                const complex_t  apc =    a + c;
                const complex_t  amc =    a - c;
                const complex_t  bpd =    b + d;
                const complex_t jbmd = jx(b - d);
                xq[p+n0] =  apc +  bpd;
                xq[p+n1] = (apc -  bpd) * w2p;
                xq[p+n2] = (amc - jbmd) * w1p;
                xq[p+n3] = (amc + jbmd) * w3p;
            }
        }
    }
    if (n == 2) {
        for (int q = 0; q < N; q += 2) {
            complex_t* const xq = x + q;
            const complex_t a = xq[0];
            const complex_t b = xq[1];
            xq[0] = a + b;
            xq[1] = a - b;
        }
    }
}

static inline void invbut(int N, complex_t* x, complex_t* W)
{
    int n = N;
    for (int s = 1; n > 2; n /= 4, s *= 4) {
        const int n0 = 0;
        const int n1 = n / 4;
        const int n2 = n / 2;
        const int n3 = n1 + n2;
        for (int q = 0; q < N; q += n) {
            complex_t* const xq = x + q;
            for (int p = 0; p < n1; p++) {
                const complex_t w1p = W[1*s*p];
                const complex_t w2p = W[2*s*p];
                const complex_t w3p = W[3*s*p];
                const complex_t a = xq[p+n0];
                const complex_t b = xq[p+n1];
                const complex_t c = xq[p+n2];
                const complex_t d = xq[p+n3];
                const complex_t  apc =    a + c;
                const complex_t  amc =    a - c;
                const complex_t  bpd =    b + d;
                const complex_t jbmd = jx(b - d);
                xq[p+n0] =  apc +  bpd;
                xq[p+n1] = (apc -  bpd) * w2p;
                xq[p+n2] = (amc + jbmd) * w1p;
                xq[p+n3] = (amc - jbmd) * w3p;
            }
        }
    }
    if (n == 2) {
        for (int q = 0; q < N; q += 2) {
            complex_t* const xq = x + q;
            const complex_t a = xq[0];
            const complex_t b = xq[1];
            xq[0] = a + b;
            xq[1] = a - b;
        }
    }
}

struct FFT
{
    int N;
    simd_array<complex_t> a_W;
    simd_array<int> a_bitrev;
    complex_t* W;
    int* bitrev;

    FFT(int n) : N(n), a_W(N), a_bitrev(N), W(&a_W), bitrev(&a_bitrev)
    {
        const double theta0 = 2*M_PI/N;
        for (int p = 0; p < N; p++) {
            const double theta = p * theta0;
            W[p] = complex_t(cos(theta), sin(theta));
        }
        bitrev[0] = 0; bitrev[N-1] = N-1;
        for (int i = 0, j = 1; j < N-1; j++) {
            for (int k = N >> 1; k > (i ^= k); k >>= 1);
            bitrev[j] = i;
        }
    }

    void fwd(complex_t* x)
    {
        fwdbut(N, x, W);
        const double rN = 1.0 / N;
        for (int p = 0; p < N; p++) {
            x[p] *= rN;
            const int q = bitrev[p];
            if (p > q) std::swap(x[p], x[q]);
        }
    }

    void inv(complex_t* x)
    {
        invbut(N, x, W);
        for (int p = 0; p < N; p++) {
            const int q = bitrev[p];
            if (p > q) std::swap(x[p], x[q]);
        }
    }
};

} /////////////////////////////////////////////////////////////////////////////

#endif // simple_fft_h
