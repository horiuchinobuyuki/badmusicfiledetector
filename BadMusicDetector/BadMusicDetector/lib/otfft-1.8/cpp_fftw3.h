/******************************************************************************
*  FFTW3 using C++
******************************************************************************/

#ifndef cpp_fftw3_h
#define cpp_fftw3_h

#include <new>
#include <fftw3.h>
#include <omp.h>
#include "fftmisc.h"

#define FFTW_MT_THRESHOLD (1<<14)

#define FFTW_FLAG FFTW_ESTIMATE
//#define FFTW_FLAG FFTW_MEASURE
//#define FFTW_FLAG FFTW_PATIENT
//#define FFTW_FLAG FFTW_EXHAUSTIVE

namespace FFTW { //////////////////////////////////////////////////////////////

using namespace fftmisc;

class FFTPlan
{
private:
    const int N;
    simd_array<complex_t> a_y;
    complex_t* x;
    complex_t* y;
    fftw_plan p;

public:
    FFTPlan(int N, complex_t * const x, int flag = FFTW_FLAG) : N(N), a_y(N), x(x), y(&a_y)
    {
        if (N >= FFTW_MT_THRESHOLD) fftw_plan_with_nthreads(omp_get_max_threads());
        p = fftw_plan_dft_1d(N,
            reinterpret_cast<fftw_complex*>(x),
            reinterpret_cast<fftw_complex*>(y),
            FFTW_FORWARD, flag);
        if (!p) throw std::bad_alloc();
    }

    ~FFTPlan() { fftw_destroy_plan(p); }

    void operator ()()
    {
        fftw_execute(p);
        const ymm rN = cmplx2(1.0/N, 1.0/N, 1.0/N, 1.0/N);
        if (N < 2)
            x[0] = y[0];
        else
            for (int k = 0; k < N; k += 2) setpz2(x+k, mulpd2(rN, getpz2(y+k)));
    }
};

class IFFTPlan
{
private:
    int N;
    simd_array<complex_t> a_y;
    complex_t* x;
    complex_t* y;
    fftw_plan p;

public:
    IFFTPlan(int N, complex_t* const x, int flag = FFTW_FLAG) : N(N), a_y(N), x(x), y(&a_y)
    {
        if (N >= FFTW_MT_THRESHOLD) fftw_plan_with_nthreads(omp_get_max_threads());
        p = fftw_plan_dft_1d(N,
            reinterpret_cast<fftw_complex*>(x),
            reinterpret_cast<fftw_complex*>(y),
            FFTW_BACKWARD, flag);
        if (!p) throw std::bad_alloc();
    }

    ~IFFTPlan() { fftw_destroy_plan(p); }

    void operator ()()
    {
        fftw_execute(p);
        if (N < 2)
            x[0] = y[0];
        else
            for (int k = 0; k < N; k += 2) setpz2(x+k, getpz2(y+k));
    }
};

} /////////////////////////////////////////////////////////////////////////////

#endif // cpp_fftw3_h
