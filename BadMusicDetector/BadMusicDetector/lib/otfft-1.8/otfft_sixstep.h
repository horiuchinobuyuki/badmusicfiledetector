/******************************************************************************
*  OTFFT Sixstep Version 1.8
******************************************************************************/

#ifndef otfft_sixstep_h
#define otfft_sixstep_h

#include <cmath>
#include <algorithm>
//#include "otfft_difavx.h"
#include "otfft_ditavx.h"

#define OMP_THRESHOLD_SS (1<<13)

namespace OTFFT_Sixstep { /////////////////////////////////////////////////////

using namespace fftmisc;

template <int log_N> struct fwdfft1
{
    static const int log_n = log_N / 2;
    static const int N = 1 << log_N;
    static const int n = 1 << log_n;
    
    template <class fft_t>
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W, const fft_t& fft1, const fft_t& fft2)
    {
        if (N < OMP_THRESHOLD_SS) {
            for (int p = 0; p < n; p++) {
                for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
            }
            for (int p = 0; p < n; p++) {
                fft1.fwd0(x + p*n, y + p*n);
                //for (int k = 0; k < n; k++) setpz(x[k + p*n], mulpz(getpz(W[N-k*p]), getpz(x[k + p*n])));
                for (int k = 0; k < n; k += 2) {
                    const ymm wkp = catlohi(getpz(W[N-k*p]), getpz(W[N-k*p-p]));
                    setpz2(x+k+p*n, mulpz2(wkp, getpz2(x+k+p*n)));
                }
            }
            for (int p = 0; p < n; p++) {
                for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
            }
            for (int k = 0; k < n; k++) { fft2.fwd0(x + k*n, y + k*n); }
            for (int p = 0; p < n; p++) {
                for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
            }
        }
        else {
            #pragma omp parallel
            {
                #pragma omp for
                for (int p = 0; p < n; p++) {
                    for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
                }
                #pragma omp for
                for (int p = 0; p < n; p++) {
                    fft1.fwd0(x + p*n, y + p*n);
                    //for (int k = 0; k < n; k++) setpz(x[k + p*n], mulpz(getpz(W[N-k*p]), getpz(x[k + p*n])));
                    for (int k = 0; k < n; k += 2) {
                        const ymm wkp = catlohi(getpz(W[N-k*p]), getpz(W[N-k*p-p]));
                        setpz2(x+k+p*n, mulpz2(wkp, getpz2(x+k+p*n)));
                    }
                }
                #pragma omp for
                for (int p = 0; p < n; p++) {
                    for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
                }
                #pragma omp for
                for (int k = 0; k < n; k++) { fft2.fwd0(x + k*n, y + k*n); }
                #pragma omp for
                for (int p = 0; p < n; p++) {
                    for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
                }
            }
        }
    }
};

template <int log_N> struct invfft1
{
    static const int log_n = log_N / 2;
    static const int N = 1 << log_N;
    static const int n = 1 << log_n;
    
    template <class fft_t>
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W, const fft_t& fft1, const fft_t& fft2)
    {
        if (N < OMP_THRESHOLD_SS) {
            for (int p = 0; p < n; p++) {
                for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
            }
            for (int p = 0; p < n; p++) {
                fft1.inv0(x + p*n, y + p*n);
                //for (int k = 0; k < n; k++) setpz(x[k + p*n], mulpz(getpz(W[k*p]), getpz(x[k + p*n])));
                for (int k = 0; k < n; k += 2) {
                    const ymm wkp = catlohi(getpz(W[k*p]), getpz(W[k*p+p]));
                    setpz2(x+k+p*n, mulpz2(wkp, getpz2(x+k+p*n)));
                }
            }
            for (int p = 0; p < n; p++) {
                for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
            }
            for (int k = 0; k < n; k++) { fft2.inv0(x + k*n, y + k*n); }
            for (int p = 0; p < n; p++) {
                for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
            }
        }
        else {
            #pragma omp parallel
            {
                #pragma omp for
                for (int p = 0; p < n; p++) {
                    for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
                }
                #pragma omp for
                for (int p = 0; p < n; p++) {
                    fft1.inv0(x + p*n, y + p*n);
                    //for (int k = 0; k < n; k++) setpz(x[k + p*n], mulpz(getpz(W[k*p]), getpz(x[k + p*n])));
                    for (int k = 0; k < n; k += 2) {
                        const ymm wkp = catlohi(getpz(W[k*p]), getpz(W[k*p+p]));
                        setpz2(x+k+p*n, mulpz2(wkp, getpz2(x+k+p*n)));
                    }
                }
                #pragma omp for
                for (int p = 0; p < n; p++) {
                    for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
                }
                #pragma omp for
                for (int k = 0; k < n; k++) { fft2.inv0(x + k*n, y + k*n); }
                #pragma omp for
                for (int p = 0; p < n; p++) {
                    for (int k = 0; k < p; k++) std::swap(x[k + p*n], x[p + k*n]);
                }
            }
        }
    }
};

template <int log_N> struct fwdfft2
{
    static const int log_n = log_N / 2;
    static const int log_m = log_N - log_n;
    static const int N = 1 << log_N;
    static const int n = 1 << log_n;
    static const int m = 1 << log_m;
    
    template <class fft_t>
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W, const fft_t& fft1, const fft_t& fft2)
    {
        if (N < OMP_THRESHOLD_SS) {
            for (int p = 0; p < m; p++) {
                for (int k = 0; k < n; k++) setpz(y[k + p*n], getpz(x[p + k*m]));
            }
            for (int p = 0; p < m; p++) {
                fft1.fwd0(y + p*n, x + p*n);
            }
            for (int p = 0; p < m; p++) {
                for (int k = 0; k < n; k++) setpz(x[p + k*m], mulpz(getpz(W[N-k*p]), getpz(y[k + p*n])));
            }
            for (int k = 0; k < n; k++) { fft2.fwd0(x + k*m, y + k*m); }
            for (int p = 0; p < m; p++) {
                for (int k = 0; k < n; k++) setpz(y[k + p*n], getpz(x[p + k*m]));
            }
            for (int p = 0; p < N; p += 2) { setpz2(x+p, getpz2(y+p)); }
        }
        else {
            #pragma omp parallel
            {
                #pragma omp for
                for (int p = 0; p < m; p++) {
                    for (int k = 0; k < n; k++) setpz(y[k + p*n], getpz(x[p + k*m]));
                }
                #pragma omp for
                for (int p = 0; p < m; p++) {
                    fft1.fwd0(y + p*n, x + p*n);
                }
                #pragma omp for
                for (int p = 0; p < m; p++) {
                    for (int k = 0; k < n; k++) setpz(x[p + k*m], mulpz(getpz(W[N-k*p]), getpz(y[k + p*n])));
                }
                #pragma omp for
                for (int k = 0; k < n; k++) { fft2.fwd0(x + k*m, y + k*m); }
                #pragma omp for
                for (int p = 0; p < m; p++) {
                    for (int k = 0; k < n; k++) setpz(y[k + p*n], getpz(x[p + k*m]));
                }
                #pragma omp for
                for (int p = 0; p < N; p += 2) { setpz2(x+p, getpz2(y+p)); }
            }
        }
    }
};

template <int log_N> struct invfft2
{
    static const int log_n = log_N / 2;
    static const int log_m = log_N - log_n;
    static const int N = 1 << log_N;
    static const int n = 1 << log_n;
    static const int m = 1 << log_m;
    
    template <class fft_t>
    inline void operator()(complex_vector x, complex_vector y, const_complex_vector W, const fft_t& fft1, const fft_t& fft2)
    {
        if (N < OMP_THRESHOLD_SS) {
            for (int p = 0; p < m; p++) {
                for (int k = 0; k < n; k++) setpz(y[k + p*n], getpz(x[p + k*m]));
            }
            for (int p = 0; p < m; p++) {
                fft1.inv0(y + p*n, x + p*n);
            }
            for (int p = 0; p < m; p++) {
                for (int k = 0; k < n; k++) setpz(x[p + k*m], mulpz(getpz(W[k*p]), getpz(y[k + p*n])));
            }
            for (int k = 0; k < n; k++) { fft2.inv0(x + k*m, y + k*m); }
            for (int p = 0; p < m; p++) {
                for (int k = 0; k < n; k++) setpz(y[k + p*n], getpz(x[p + k*m]));
            }
            for (int p = 0; p < N; p += 2) { setpz2(x+p, getpz2(y+p)); }
        }
        else {
            #pragma omp parallel
            {
                #pragma omp for
                for (int p = 0; p < m; p++) {
                    for (int k = 0; k < n; k++) setpz(y[k + p*n], getpz(x[p + k*m]));
                }
                #pragma omp for
                for (int p = 0; p < m; p++) {
                    fft1.inv0(y + p*n, x + p*n);
                }
                #pragma omp for
                for (int p = 0; p < m; p++) {
                    for (int k = 0; k < n; k++) setpz(x[p + k*m], mulpz(getpz(W[k*p]), getpz(y[k + p*n])));
                }
                #pragma omp for
                for (int k = 0; k < n; k++) { fft2.inv0(x + k*m, y + k*m); }
                #pragma omp for
                for (int p = 0; p < m; p++) {
                    for (int k = 0; k < n; k++) setpz(y[k + p*n], getpz(x[p + k*m]));
                }
                #pragma omp for
                for (int p = 0; p < N; p += 2) { setpz2(x+p, getpz2(y+p)); }
            }
        }
    }
};

/*****************************************************************************/

struct FFT0
{
    int N, log_N;
    simd_array<complex_t> a_W;
    complex_t* W;
    //OTFFT_DIFAVX::FFT0 fft1, fft2;
    OTFFT_DITAVX::FFT0 fft1, fft2;

    FFT0() : N(0), log_N(0), W(0) {}
    FFT0(int n)
    {
        for (log_N = 0; n > 1; n >>= 1) log_N++;
        setup2(log_N);
    }

    void setup(int n)
    {
        for (log_N = 0; n > 1; n >>= 1) log_N++;
        setup2(log_N);
    }

    void setup2(const int n)
    {
        log_N = n; N = 1 << n;
        a_W.setup(N+1); W = &a_W;
        if (N < 4) fft1.setup2(n);
        else { fft1.setup2(n/2); fft2.setup2(n - n/2); }
        const double theta0 = 2*M_PI/N;
#if 0
        for (int p = 0; p <= N/2; p++) {
            const double theta = p * theta0;
            const double c = cos(theta);
            const double s = sin(theta);
            W[p] = complex_t(c, s);
            W[N-p] = complex_t(c, -s);
        }
#else
        const int Nh = N/2;
        const int Nq = N/4;
        const int Ne = N/8;
        const int Nd = N - Nq;
        if (N < 2) {}
        else if (N < 4) { W[0] = W[2] = 1; W[1] = complex_t(-1, 0); }
        else if (N < 16) for (int p = 0; p <= Nq; p++) {
            const double theta = p * theta0;
            const double c = cos(theta);
            const double s = sin(theta);
            W[p] = complex_t(c, s);
            W[Nq+p] = complex_t(-s, c);
            W[N-p] = complex_t(c, -s);
            W[Nd-p] = complex_t(-s, -c);
        }
        else for (int p = 0; p <= Ne; p++) {
            const double theta = p * theta0;
            const double c = cos(theta);
            const double s = sin(theta);
            W[p] = complex_t(c, s);
            W[Nq-p] = complex_t(s, c);
            W[Nq+p] = complex_t(-s, c);
            W[Nh-p] = complex_t(-c, s);
            W[N-p] = complex_t(c, -s);
            W[Nd+p] = complex_t(s, -c);
            W[Nd-p] = complex_t(-s, -c);
            W[Nh+p] = complex_t(-c, -s);
        }
#endif
    }

    inline void fwd(complex_vector x, complex_vector y) const
    {
        if (N < 2) return;
        switch (log_N) {
        case  1: fft1.fwd0(x, y); break;
        case  2: fwdfft1< 2>()(x, y, W, fft1, fft2); break;
        case  3: fwdfft2< 3>()(x, y, W, fft1, fft2); break;
        case  4: fwdfft1< 4>()(x, y, W, fft1, fft2); break;
        case  5: fwdfft2< 5>()(x, y, W, fft1, fft2); break;
        case  6: fwdfft1< 6>()(x, y, W, fft1, fft2); break;
        case  7: fwdfft2< 7>()(x, y, W, fft1, fft2); break;
        case  8: fwdfft1< 8>()(x, y, W, fft1, fft2); break;
        case  9: fwdfft2< 9>()(x, y, W, fft1, fft2); break;
        case 10: fwdfft1<10>()(x, y, W, fft1, fft2); break;
        case 11: fwdfft2<11>()(x, y, W, fft1, fft2); break;
        case 12: fwdfft1<12>()(x, y, W, fft1, fft2); break;
        case 13: fwdfft2<13>()(x, y, W, fft1, fft2); break;
        case 14: fwdfft2<14>()(x, y, W, fft1, fft2); break;
        case 15: fwdfft2<15>()(x, y, W, fft1, fft2); break;
        case 16: fwdfft1<16>()(x, y, W, fft1, fft2); break;
        case 17: fwdfft2<17>()(x, y, W, fft1, fft2); break;
        case 18: fwdfft1<18>()(x, y, W, fft1, fft2); break;
        case 19: fwdfft2<19>()(x, y, W, fft1, fft2); break;
        case 20: fwdfft1<20>()(x, y, W, fft1, fft2); break;
        case 21: fwdfft2<21>()(x, y, W, fft1, fft2); break;
        case 22: fwdfft1<22>()(x, y, W, fft1, fft2); break;
        case 23: fwdfft2<23>()(x, y, W, fft1, fft2); break;
        case 24: fwdfft1<24>()(x, y, W, fft1, fft2); break;
        }
        const ymm rN = cmplx2(1.0/N, 1.0/N, 1.0/N, 1.0/N);
        for (int p = 0; p < N; p += 2) setpz2(x+p, mulpd2(rN, getpz2(x+p)));
    }

    inline void fwd0(complex_vector x, complex_vector y) const
    {
        if (N < 2) return;
        switch (log_N) {
        case  1: fft1.fwd0(x, y); break;
        case  2: fwdfft1< 2>()(x, y, W, fft1, fft2); break;
        case  3: fwdfft2< 3>()(x, y, W, fft1, fft2); break;
        case  4: fwdfft1< 4>()(x, y, W, fft1, fft2); break;
        case  5: fwdfft2< 5>()(x, y, W, fft1, fft2); break;
        case  6: fwdfft1< 6>()(x, y, W, fft1, fft2); break;
        case  7: fwdfft2< 7>()(x, y, W, fft1, fft2); break;
        case  8: fwdfft1< 8>()(x, y, W, fft1, fft2); break;
        case  9: fwdfft2< 9>()(x, y, W, fft1, fft2); break;
        case 10: fwdfft1<10>()(x, y, W, fft1, fft2); break;
        case 11: fwdfft2<11>()(x, y, W, fft1, fft2); break;
        case 12: fwdfft1<12>()(x, y, W, fft1, fft2); break;
        case 13: fwdfft2<13>()(x, y, W, fft1, fft2); break;
        case 14: fwdfft2<14>()(x, y, W, fft1, fft2); break;
        case 15: fwdfft2<15>()(x, y, W, fft1, fft2); break;
        case 16: fwdfft1<16>()(x, y, W, fft1, fft2); break;
        case 17: fwdfft2<17>()(x, y, W, fft1, fft2); break;
        case 18: fwdfft1<18>()(x, y, W, fft1, fft2); break;
        case 19: fwdfft2<19>()(x, y, W, fft1, fft2); break;
        case 20: fwdfft1<20>()(x, y, W, fft1, fft2); break;
        case 21: fwdfft2<21>()(x, y, W, fft1, fft2); break;
        case 22: fwdfft1<22>()(x, y, W, fft1, fft2); break;
        case 23: fwdfft2<23>()(x, y, W, fft1, fft2); break;
        case 24: fwdfft1<24>()(x, y, W, fft1, fft2); break;
        }
    }

    inline void inv(complex_vector x, complex_vector y) const
    {
        if (N < 2) return;
        switch (log_N) {
        case  1: fft1.inv0(x, y); break;
        case  2: invfft1< 2>()(x, y, W, fft1, fft2); break;
        case  3: invfft2< 3>()(x, y, W, fft1, fft2); break;
        case  4: invfft1< 4>()(x, y, W, fft1, fft2); break;
        case  5: invfft2< 5>()(x, y, W, fft1, fft2); break;
        case  6: invfft1< 6>()(x, y, W, fft1, fft2); break;
        case  7: invfft2< 7>()(x, y, W, fft1, fft2); break;
        case  8: invfft1< 8>()(x, y, W, fft1, fft2); break;
        case  9: invfft2< 9>()(x, y, W, fft1, fft2); break;
        case 10: invfft1<10>()(x, y, W, fft1, fft2); break;
        case 11: invfft2<11>()(x, y, W, fft1, fft2); break;
        case 12: invfft1<12>()(x, y, W, fft1, fft2); break;
        case 13: invfft2<13>()(x, y, W, fft1, fft2); break;
        case 14: invfft2<14>()(x, y, W, fft1, fft2); break;
        case 15: invfft2<15>()(x, y, W, fft1, fft2); break;
        case 16: invfft1<16>()(x, y, W, fft1, fft2); break;
        case 17: invfft2<17>()(x, y, W, fft1, fft2); break;
        case 18: invfft1<18>()(x, y, W, fft1, fft2); break;
        case 19: invfft2<19>()(x, y, W, fft1, fft2); break;
        case 20: invfft1<20>()(x, y, W, fft1, fft2); break;
        case 21: invfft2<21>()(x, y, W, fft1, fft2); break;
        case 22: invfft1<22>()(x, y, W, fft1, fft2); break;
        case 23: invfft2<23>()(x, y, W, fft1, fft2); break;
        case 24: invfft1<24>()(x, y, W, fft1, fft2); break;
        }
    }

    inline void inv0(complex_vector x, complex_vector y) const { inv(x, y); }
};

struct FFT
{
    FFT0 fft;
    simd_array<complex_t> a_y;
    complex_t* y;

    FFT() : fft(), a_y(), y(0) {}
    FFT(const int n) : fft(n), a_y(n), y(&a_y) {}

    inline void setup(const int n) { fft.setup(n); a_y.setup(n); y = &a_y; }

    inline void fwd(complex_vector x)  const { fft.fwd(x, y);  }
    inline void fwd0(complex_vector x) const { fft.fwd0(x, y); }
    inline void inv(complex_vector x)  const { fft.inv(x, y);  }
    inline void inv0(complex_vector x) const { fft.inv0(x, y); }
};

void speedup_magic(const int N = 1 << 18)
{
    const double theta0 = 2*M_PI/N;
    volatile double sum = 0;
    for (int p = 0; p < N; p++) {
        sum += cos(p * theta0);
    }
}

} /////////////////////////////////////////////////////////////////////////////

#endif // otfft_sixstep_h
