/******************************************************************************
*  FFT Benchmark 2
******************************************************************************/

#include <cstdio>
#include <cmath>
#include "stopwatch.h"
#include "fftmisc.h"
#include "cpp_fftw3.h"
#include "ooura2.h"
#include "simple_fft.h"
#include "otfft.h"
using namespace fftmisc;

#define DELAY 1

int main() try
{
    static const int n_max  = 22;
    static const int N_max  = 1 << n_max;
    static const int Nn_max = N_max * n_max;

    fftw_init_threads();
    setbuf(stdout, NULL);
    printf("------+-----------+-----------------+-----------------+-----------------\n");
    printf("length|FFTW3[usec]|   OOURA   [usec]| SimpleFFT [usec]|   OTFFT   [usec]\n");
    printf("------+-----------+-----------------+-----------------+-----------------\n");
    complex_t* x0 = (complex_t*) simd_malloc(N_max*sizeof(complex_t));
    complex_t* x1 = (complex_t*) simd_malloc(N_max*sizeof(complex_t));
    complex_t* x2 = (complex_t*) simd_malloc(N_max*sizeof(complex_t));
    complex_t* x3 = (complex_t*) simd_malloc(N_max*sizeof(complex_t));
    complex_t* x4 = (complex_t*) simd_malloc(N_max*sizeof(complex_t));
    for (int n = 4; n <= n_max; n++) {
        const int N = 1 << n;
        const int LOOPS = static_cast<int>(rint(Nn_max/double(N*n)));
        counter_t cp1, cp2;
        double lap, lap1;

        //// sample setting ////
        for (int p = 0; p < N; p++) {
            const double t = double(p) / N;
            x0[p].Re = 10 * cos((2*M_PI/N)*t*t);
            x0[p].Im = 10 * sin((2*M_PI/N)*t*t);
            x1[p] = x2[p] = x3[p] = x4[p] = x0[p];
        }
        sleep(DELAY);

        printf("2^(%2d)|", n);

        //// FFTW3 FFT ////
        cp1 = get_counter();
        for (int i = 0; i < LOOPS; i++) {
            FFTW::FFTPlan   fft_plan(N, x1);
            FFTW::IFFTPlan ifft_plan(N, x1);
            fft_plan();
            ifft_plan();
        }
        cp2 = get_counter();
        lap = usec(cp2 - cp1) / LOOPS;
        printf("%11.2f|", lap);
        sleep(DELAY);
        lap1 = lap;

        //// OOURA FFT ////
        cp1 = get_counter();
        for (int i = 0; i < LOOPS; i++) {
            OOURA::FFT ooura_fft(N);
            ooura_fft.fwd(x2);
            ooura_fft.inv(x2);
        }
        cp2 = get_counter();
        lap = usec(cp2 - cp1) / LOOPS;
        printf("%11.2f(%3.0f%%)|", lap, 100*lap/lap1);
        sleep(DELAY);

        //// Simple FFT ////
        cp1 = get_counter();
        for (int i = 0; i < LOOPS; i++) {
            SimpleFFT2::FFT simple_fft(N);
            simple_fft.fwd(x3);
            simple_fft.inv(x3);
        }
        cp2 = get_counter();
        lap = usec(cp2 - cp1) / LOOPS;
        printf("%11.2f(%3.0f%%)|", lap, 100*lap/lap1);
        sleep(DELAY);

        //// OTFFT ////
        cp1 = get_counter();
        for (int i = 0; i < LOOPS; i++) {
            OTFFT::FFT ot_fft(N);
            ot_fft.fwd(x4);
            ot_fft.inv(x4);
        }
        cp2 = get_counter();
        lap = usec(cp2 - cp1) / LOOPS;
        printf("%11.2f(%3.0f%%)\n", lap, 100*lap/lap1);
        sleep(DELAY);
    }
    simd_free(x4);
    simd_free(x3);
    simd_free(x2);
    simd_free(x1);
    simd_free(x0);
    printf("------+-----------+-----------------+-----------------+-----------------\n");

    return 0;
}
catch (std::bad_alloc&) { fprintf(stderr, "\n""not enough memory!!\n"); }
